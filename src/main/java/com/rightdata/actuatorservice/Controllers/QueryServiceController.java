package com.rightdata.actuatorservice.Controllers;

import com.rightdata.actuatorservice.POJOs.*;
import com.rightdata.actuatorservice.POJOs.Navigator.NavigationResponsePOJO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rightdata.actuatorservice.Services.DeltaService;
import com.rightdata.actuatorservice.Services.QueryHistoryService;
import com.rightdata.actuatorservice.Services.QueryService;
//import org.json.JSONObject;
import com.rightdata.actuatorservice.Utils.SparkSessionUtil;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.handler.annotation.Payload;
//import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.security.Principal;
import java.sql.*;
import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//@Scope(WebApplicationContext.SCOPE_REQUEST)
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/services")
public class QueryServiceController {
//    @Autowired
//    private LogoutService logoutService;

//    @Autowired
//    private HttpServletRequest request;

    private final QueryService queryService;
    private final DeltaService deltaService;
    private final QueryHistoryService queryHistoryService;
    private ApplicationContext applicationContext;
    private final Logger logger = LoggerFactory.getLogger(QueryServiceController.class);

    public QueryServiceController(QueryService queryService, DeltaService deltaService, QueryHistoryService queryHistoryService) {
        this.queryService = queryService;
        this.deltaService = deltaService;
        this.queryHistoryService = queryHistoryService;
    }

//    @MessageMapping("/topic/activity")
//    @SendTo("/topic/blog")
//    public String sendActivity(@Payload String message)
//    {
//        return message;
//    }


    @RequestMapping(method = RequestMethod.POST, value = "/run-query")
    public ResponseEntity<ResponsePOJO> processSQL(@RequestBody QueryServicePOJO queryServiceString) {
        String json = null;
        return queryService.processQuery(queryServiceString);
    }
    @RequestMapping(method = RequestMethod.POST, value = "/get-query-history")
    public ResponseEntity<QueryHistoryResponsePOJO> getSqlHistory() {
        String json = null;
        return queryHistoryService.getQueryHistory();
    }

//    @RequestMapping(method = RequestMethod.GET, value = "/stream-get", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
//    public Flux<String> getstream() {
//        for (int i = 0; i < 10; i++) {
//            return Flux.interval(Duration.ofSeconds(1)).map(j -> "Naggme" + j);
//        }
//
//        return Flux.interval(Duration.ofSeconds(1)).map(j -> "Name" + j);

//        String json = null;
//        JSONObject obj = new JSONObject();
//        obj = {
//                "sourceType": "mysqlqa",
//                "source": {
//                    "type": "DELTAa",
//                    "profileId": null,
//                    "guid": "331567997d430d5ed961099f96e58d29",
//                    "connectionInfo": {
//                        "connectionJDBCURL": "jdbc:snowflake://bja71088.us-east-1.snowflakecomputing.com",
//                        "username": "dextrus",
//                        "password": "Dextrus!1",
//                        "className": "com.snowflake.client.jdbc.SnowflakeDriver",
//                        "db": "SNOWFLAKE_SAMPLE_DATA",
//                        "account": "kda35912"
//                    },
//                    "sourceInfo": {
//                        "id": "1",
//                        "sourceTableOrQuery": "select * from TPCDS_SF100TCL.CATALOG_SALES  ",
//                        "blendColumns": null,
//                        "driverTable": false
//                    },
//                    "datasetJoins": null,
//                    "purpose": "dataPreview",
//                    "skipRows": 0,
//                    "previewCount": 5001
//                }
//            };
//        StreamingResponseBody stream = queryService.streamResponse(queryServiceString);

//        ResponseEntity<ResponsePOJO> res = queryService.processQuery(queryServiceString);

//        response.setContentType("application/json");
//        StreamingResponseBody stream = out -> {
//            try {
////            try(ObjectOutputStream oos = new ObjectOutputStream(out)) {
//                for (int i = 0; i < 10; i++) {
////                    QueryServicePOJO queryServiceString =  new QueryServicePOJO();
////                        oos.write(queryServiceString.toString().getBytes());
//                        String content = "{\"counter\":" + i + "}\n";
//                    out.write(content.getBytes());
////                    out.write(queryServiceString.toString().getBytes());
////                        oos.flush();
//                        out.flush();
////                        logger.info("size: " + content.getBytes().length);
//                        Thread.sleep(1000);
//
//                }
//                out.close();
//            } catch (final Exception e) {
//                logger.error("Exception", e);
//            }
//        };
//        logger.info("steaming response {} ", stream);
//        return new ResponseEntity(stream, HttpStatus.OK);
//    }

//    @RequestMapping(method = RequestMethod.POST, value = "/stream-query", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
//    public ResponseEntity<StreamingResponseBody> processSQL(final HttpServletResponse response, @RequestBody QueryServicePOJO queryServiceString) {
//        String json = null;
//        StreamingResponseBody stream = queryService.streamResponse(queryServiceString);
//
////        ResponseEntity<ResponsePOJO> res = queryService.processQuery(queryServiceString);
//
//        response.setContentType("application/stream+json");
////        StreamingResponseBody stream = out -> {
////            try {
//////            try(ObjectOutputStream oos = new ObjectOutputStream(out)) {
////                for (int i = 0; i < 10; i++) {
////                    QueryServicePOJO queryServiceString =  new QueryServicePOJO();
//////                        oos.write(queryServiceString.toString().getBytes());
////                        String content = "{\"counter\":" + i + "}\n";
////                    out.write(content.getBytes());
////                    out.write(queryServiceString.toString().getBytes());
//////                        oos.flush();
////                        out.flush();
//////                        logger.info("size: " + content.getBytes().length);
////                        Thread.sleep(1000);
////
////                }
////                out.close();
////            } catch (final Exception e) {
////                logger.error("Exception", e);
////            }
////        };
//        logger.info("steaming response {} ", stream);
//        return new ResponseEntity(stream, HttpStatus.OK);
//    }

    @GetMapping(value = "/consume", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> consume() throws IOException {
        URL url = new URL("http://localhost:8080/services/stream-query");
        URLConnection connection = url.openConnection();
        connection.setDoOutput(true);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream())
        );

        String decodedString;
        while ((decodedString = in.readLine()) != null) {
            logger.info(decodedString);
        }

        in.close();

        return new ResponseEntity("ok", HttpStatus.OK);
    }




    @RequestMapping(method = RequestMethod.POST, value = "/check-query-syntax")
    public ResponseEntity<ResponsePOJO> checkSyntax(@RequestBody SourcePOJO sourcePOJO) {
        String json = null;
        return queryService.checkQuerySyntax(sourcePOJO);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/run-delta-query")
    public ResponseEntity<ResponsePOJO> processDelta(@RequestBody QueryServicePOJO queryServiceString) throws IOException {
        String json = null;
        return deltaService.runQuery(queryServiceString);
    }

//    @RequestMapping(method = RequestMethod.POST, value = "/delta-version")
//    public void getDeltaTableVersion(@RequestBody QueryServicePOJO queryServiceString) throws IOException {
//        String json = null;
//        deltaService.getVersionHistory(queryServiceString);
//    }

    @RequestMapping(method = RequestMethod.POST, value = "/delta-navigation")
    public ResponseEntity<NavigationResponsePOJO> listDatabasesDelta(@RequestBody QueryServicePOJO queryServiceString) throws IOException {
        String json = null;
        return deltaService.getNavigatorList(queryServiceString);
    }

//    @RequestMapping(method = RequestMethod.POST, value = "/logout")
//    public boolean logout() {
//        String auth = request.getHeader("Authorization");
//        System.out.println(auth);
//
//
//        return logoutService.logout(auth, "venki");
//    }

    // for sql injections
    /*
    for sql injections

    private String dmlWithSQLInjections(String sourceType, String connectionJDBCURL, String username, String password, String className, String sourceTableOrQuery) {
        JSONObject output=new JSONObject();
        try {
            Connection conn = connectToRemoteDB(connectionJDBCURL, username, password, className);
            DatabaseMetaData meta = conn.getMetaData();
            PreparedStatement pstmt = null;
            int rowsAffected =0;
            String sql="";
            String inputSql = sourceTableOrQuery.trim().toLowerCase().replace("\n", " ");
            String dmlOperation = inputSql.substring(0, inputSql.indexOf(" "));
            switch (dmlOperation){
                case "insert" :
                    String tableName=getTableName(inputSql,"insert");
                    JSONObject columnCSV=getColumnCSV(inputSql,"insert",tableName,meta);
                    JSONObject inputValueAndFormat=getInputValueAndFormat(inputSql,"insert");
                    sql="insert into "+tableName+columnCSV.getString("columnCSV")+" values "+inputValueAndFormat.get("inputFormat")+";";
                    pstmt=conn.prepareStatement(sql);
                    ArrayList<JSONObject> columns= (ArrayList<JSONObject>) columnCSV.get("columnMetaData");
                    String[] inputValues= (String[]) inputValueAndFormat.get("inputValues");
                    int i=0;
                    for (JSONObject column:columns) {
                        System.out.println(column.getString("COLUMN_NAME"));
                        System.out.println(column.getString("TYPE_NAME"));
                        if(column.getString("TYPE_NAME").toLowerCase().equals("text")) {
                            pstmt.setString(i, inputValues[i]);
                            System.out.println(inputValues[i]);
                        }
                        i++;
                    }
                    rowsAffected=pstmt.executeUpdate();
                    break;
                case "update" : break;
                case "delete" : break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return "empty";
    }

    private String getTableName(String inputSql, String dmlOperation) {
        if(dmlOperation.equals("insert")){
            Pattern[] patterns ={Pattern.compile("into (.*?) \\("),Pattern.compile("into (.*?)\\("),Pattern.compile("into (.*?) values")};
            List<Matcher> matcher=new ArrayList<Matcher>();
            for (Pattern p: patterns) {
                matcher.add(p.matcher(inputSql));
            }
            for (Matcher m: matcher) {
                if(m.find()){
                    return m.group(1);
                }
            }
        }
        return null;
    }

    private JSONObject getColumnCSV(String inputSql, String dmlOperation, String tableName,DatabaseMetaData meta) throws SQLException, JSONException {
        JSONObject response=new JSONObject();
        ResultSet rsColumns = meta.getColumns(null, null, tableName, null);
        List<JSONObject> columns = new ArrayList<JSONObject>();
        List<JSONObject> finalColumns = new ArrayList<JSONObject>();
        while(rsColumns.next()) {
            JSONObject column=new JSONObject();
            column.put("COLUMN_NAME",rsColumns.getString("COLUMN_NAME"));
            column.put("TYPE_NAME",rsColumns.getString("TYPE_NAME"));
            System.out.println(column.toString());
            columns.add(column);
        }

        if(dmlOperation.equals("insert")){
            StringBuilder columnCSV= new StringBuilder("(");
            String matchedColumns="";
            Pattern[] patterns ={Pattern.compile(tableName+"\\((.*?)\\)"),Pattern.compile(tableName+" \\((.*?)\\)")};
            List<Matcher> matcher=new ArrayList<Matcher>();
            for (Pattern p: patterns) {
                matcher.add(p.matcher(inputSql));
            }
            for (Matcher m: matcher) {
                if(m.find()){
                    matchedColumns = m.group(1);
                    break;
                }
            }
            if(!matchedColumns.equals("")) {
                matchedColumns = matchedColumns.replaceAll(" ", "");
                String[] columnLiterals = matchedColumns.split(",");
                for (String column : columnLiterals) {
                    columnCSV.append(column).append(",");
                    for (JSONObject c: columns) {
                        if(c.getString("COLUMN_NAME").toLowerCase().equals(column))
                            finalColumns.add(c);
                    }
                }
                columnCSV.deleteCharAt(columnCSV.lastIndexOf(","));
                columnCSV.append(")");
                response.put("columnCSV",columnCSV.toString());
                response.put("columnMetaData",finalColumns);
                return response;
            }
        }
        return null;
    }

    private JSONObject getInputValueAndFormat(String inputSql, String dmlOperation) throws JSONException {
        if(dmlOperation.equals("insert")){
            JSONObject response = new JSONObject();
            StringBuilder inputFormat= new StringBuilder("(");
            String[] inputValues;
            String matchedValues="";
            Pattern[] patterns ={Pattern.compile(" values\\((.*?)\\)"),Pattern.compile(" values \\((.*?)\\)")};
            List<Matcher> matcher=new ArrayList<Matcher>();
            for (Pattern p: patterns) {
                matcher.add(p.matcher(inputSql));
            }
            for (Matcher m: matcher) {
                if(m.find()){
                    matchedValues = m.group(1);
                    break;
                }
            }
            if(!matchedValues.equals("")) {

                inputValues = matchedValues.split(",");
                for (Object v : inputValues) {
                    inputFormat.append("?").append(",");
                }
                inputFormat.deleteCharAt(inputFormat.lastIndexOf(","));
                inputFormat.append(")");
                try {
                    response.put("inputFormat", inputFormat);
                    response.put("inputValues", inputValues);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
                return response;
            }
        }
        return null;
    }

    */

    @RequestMapping(method = RequestMethod.POST, value = "/check-delta")
    public ResponseEntity<String> checkDelta() throws AnalysisException {
        SparkSessionUtil.sparkSession.sql("create table tb1 (c1 int) using delta");
        SparkSessionUtil.sparkSession.sql("insert into tb1 values (1)");
        Dataset<Row> dataset = SparkSessionUtil.sparkSession.sql("select * from tb1");
        dataset.createGlobalTempView("temp_view");
        SparkSession sparkSession =  SparkSession.builder()
                .master("local[*]")
                .config("spark.delta.logStore.class", "org.apache.spark.sql.delta.storage.HDFSLogStore")
//				.config("hive.metastore.warehouse.dir", warehouseLocation)
//                .config("spark.sql.warehouse.dir", "s3a://dextrus.dec.eks/user/hive/warehouse")
//                .config("spark.hadoop.fs.s3a.access.key","AKIASJLZFCYLKGINFHWS")
//                .config("spark.hadoop.fs.s3a.secret.key", "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")
                .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
                .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
                .getOrCreate().newSession();

        System.out.println(sparkSession);

        return ResponseEntity.ok("done");
    }


}