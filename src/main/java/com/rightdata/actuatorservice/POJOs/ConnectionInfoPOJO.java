package com.rightdata.actuatorservice.POJOs;

public class ConnectionInfoPOJO {
    private String connectionJDBCURL;

    private String username;

    private String password;

    private String className;

    private String schema;

    private String catalog;

    private String db;

    private String role;

    private String warehouse;

    private String account;

    public void setConnectionJDBCURL(String connectionJDBCURL){
        this.connectionJDBCURL = connectionJDBCURL;
    }
    public String getConnectionJDBCURL(){
        return this.connectionJDBCURL;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public String getUsername(){
        return this.username;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public String getPassword(){
        return this.password;
    }
    public void setClassName(String className){
        this.className = className;
    }
    public String getClassName(){
        return this.className;
    }
    public void setSchema(String schema){
        this.schema = schema;
    }
    public String getSchema(){
        return this.schema;
    }
    public void setCatalog(String catalog){
        this.catalog = catalog;
    }
    public String getCatalog(){
        return this.catalog;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public void setRole(String role) {
        this.role = role;
    }
    public String getWarehouse() {
        return warehouse;
    }
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }

    public String getRole() {
        return role;
    }
}
