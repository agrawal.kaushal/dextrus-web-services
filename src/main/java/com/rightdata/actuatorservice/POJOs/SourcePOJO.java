package com.rightdata.actuatorservice.POJOs;

public class  SourcePOJO {
    private String type;

    private String profileId;

    private String guid;

    private ConnectionInfoPOJO connectionInfo;

    private SourceInfoPOJO sourceInfo;

    private String datasetJoins;

    private String purpose;

    private int skipRows;

    private int previewCount;

    private int givenSkipRows;

    private int givenPreviewCount;

    private String searchPattern;

    public void setType(String type){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }
    public void setProfileId(String profileId){
        this.profileId = profileId;
    }
    public String getProfileId(){
        return this.profileId;
    }
    public void setGuid(String guid){
        this.guid = guid;
    }
    public String getGuid(){
        return this.guid;
    }
    public void setConnectionInfo(ConnectionInfoPOJO connectionInfo){
        this.connectionInfo = connectionInfo;
    }
    public ConnectionInfoPOJO getConnectionInfo(){
        return this.connectionInfo;
    }
    public void setSourceInfo(SourceInfoPOJO sourceInfo){
        this.sourceInfo = sourceInfo;
    }
    public SourceInfoPOJO getSourceInfo(){
        return this.sourceInfo;
    }
    public void setDatasetJoins(String datasetJoins){
        this.datasetJoins = datasetJoins;
    }
    public String getDatasetJoins(){
        return this.datasetJoins;
    }
    public void setPurpose(String purpose){
        this.purpose = purpose;
    }
    public String getPurpose(){
        return this.purpose;
    }
    public void setSkipRows(int skipRows){
        this.skipRows = skipRows;
    }
    public int getSkipRows(){
        return this.skipRows;
    }
    public void setPreviewCount(int previewCount){
        this.previewCount = previewCount;
    }
    public int getPreviewCount(){
        return this.previewCount;
    }

    public String getSearchPattern() {
        return searchPattern;
    }

    public int getGivenSkipRows() {
        return givenSkipRows;
    }

    public void setGivenSkipRows(int givenSkipRows) {
        this.givenSkipRows = givenSkipRows;
    }

    public int getGivenPreviewCount() {
        return givenPreviewCount;
    }

    public void setGivenPreviewCount(int givenPreviewCount) {
        this.givenPreviewCount = givenPreviewCount;
    }

    public void setSearchPattern(String searchPattern) {
        this.searchPattern = searchPattern;
    }
}
