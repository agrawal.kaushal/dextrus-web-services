package com.rightdata.actuatorservice.POJOs;

import java.io.Serializable;

public class QueryServicePOJO implements Serializable {
    private String sourceType;

    private SourcePOJO source;

    public void setSourceType(String sourceType){
        this.sourceType = sourceType;
    }
    public String getSourceType(){
        return this.sourceType;
    }
    public void setSource(SourcePOJO source){
        this.source = source;
    }
    public SourcePOJO getSource(){
        return this.source;
    }
}

