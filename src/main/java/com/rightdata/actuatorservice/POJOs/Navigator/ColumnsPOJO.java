package com.rightdata.actuatorservice.POJOs.Navigator;

public class ColumnsPOJO {
    public String name;
    public String systemDataType;
    public String length;
    public String precision;
    public String nullable;
    public boolean primaryKey;
    public boolean foreignKey;
    public String foreignKeyDetails;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSystemDataType() {
        return systemDataType;
    }

    public void setSystemDataType(String systemDataType) {
        this.systemDataType = systemDataType;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public String getNullable() {
        return nullable;
    }

    public void setNullable(String nullable) {
        this.nullable = nullable;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public boolean isForeignKey() {
        return foreignKey;
    }

    public void setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
    }

    public String getForeignKeyDetails() {
        return foreignKeyDetails;
    }

    public void setForeignKeyDetails(String foreignKeyDetails) {
        this.foreignKeyDetails = foreignKeyDetails;
    }
}
