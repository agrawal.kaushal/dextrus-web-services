package com.rightdata.actuatorservice.POJOs.Navigator;

import java.util.List;

public class NavigationResponsePOJO {
    public boolean status;
    public List<CatalogsPOJO> catalogs;
    public List<SchemasPOJO> schemas;
    public List<TablesPOJO> tables;
    public List<ViewsPOJO> views;
    public List<VersionsPOJO> versions;
    public List<ColumnsPOJO> columns;
    public List<SearchResultPOJO> searchResults;
    public String data;
    public String technicalMessage;
    public String userMessage;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<CatalogsPOJO> getCatalogs() {
        return catalogs;
    }

    public void setCatalogs(List<CatalogsPOJO> catalogs) {
        this.catalogs = catalogs;
    }

    public List<SchemasPOJO> getSchemas() {
        return schemas;
    }

    public void setSchemas(List<SchemasPOJO> schemas) {
        this.schemas = schemas;
    }

    public List<TablesPOJO> getTables() {
        return tables;
    }

    public void setTables(List<TablesPOJO> tables) {
        this.tables = tables;
    }

    public List<ViewsPOJO> getViews() {
        return views;
    }

    public void setViews(List<ViewsPOJO> views) {
        this.views = views;
    }

    public List<VersionsPOJO> getVersions() {
        return versions;
    }

    public void setVersions(List<VersionsPOJO> versions) {
        this.versions = versions;
    }

    public List<ColumnsPOJO> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnsPOJO> columns) {
        this.columns = columns;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTechnicalMessage() {
        return technicalMessage;
    }

    public void setTechnicalMessage(String technicalMessage) {
        this.technicalMessage = technicalMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public List<SearchResultPOJO> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(List<SearchResultPOJO> searchResults) {
        this.searchResults = searchResults;
    }
}
