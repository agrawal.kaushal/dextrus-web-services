package com.rightdata.actuatorservice.POJOs.Navigator;

public class SearchResultPOJO {
    public String CATALOG_NAME;
    public String tableName;
    public Boolean isDelta;

    public String getCATALOG_NAME() {
        return CATALOG_NAME;
    }

    public void setCATALOG_NAME(String CATALOG_NAME) {
        this.CATALOG_NAME = CATALOG_NAME;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Boolean getDelta() {
        return isDelta;
    }

    public void setDelta(Boolean delta) {
        isDelta = delta;
    }
}
