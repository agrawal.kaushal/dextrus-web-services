package com.rightdata.actuatorservice.POJOs.Navigator;

public class VersionsPOJO {
    public String versionName;
    public String timestamp;

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
