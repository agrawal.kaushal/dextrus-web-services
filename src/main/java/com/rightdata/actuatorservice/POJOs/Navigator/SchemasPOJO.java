package com.rightdata.actuatorservice.POJOs.Navigator;

public class SchemasPOJO {
    public String schemaName;

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }
}
