package com.rightdata.actuatorservice.POJOs.Navigator;

public class ViewsPOJO {
    public String viewName;

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }
}
