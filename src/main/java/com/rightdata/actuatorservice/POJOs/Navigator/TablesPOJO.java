package com.rightdata.actuatorservice.POJOs.Navigator;

public class TablesPOJO {
    public String tableName;
    public String provider;
    public Boolean isDelta;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Boolean getDelta() {
        return isDelta;
    }

    public void setDelta(Boolean delta) {
        isDelta = delta;
    }
}
