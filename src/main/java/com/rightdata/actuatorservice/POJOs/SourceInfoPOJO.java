package com.rightdata.actuatorservice.POJOs;

public class SourceInfoPOJO {
    private String id;

    private String sourceTableOrQuery;

    private String blendColumns;

    private boolean driverTable;

    private String version;

    private String table;

    private String database;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setSourceTableOrQuery(String sourceTableOrQuery){
        this.sourceTableOrQuery = sourceTableOrQuery;
    }
    public String getSourceTableOrQuery(){
        return this.sourceTableOrQuery;
    }
    public void setBlendColumns(String blendColumns){
        this.blendColumns = blendColumns;
    }
    public String getBlendColumns(){
        return this.blendColumns;
    }
    public void setDriverTable(boolean driverTable){
        this.driverTable = driverTable;
    }
    public boolean getDriverTable(){
        return this.driverTable;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }
}
