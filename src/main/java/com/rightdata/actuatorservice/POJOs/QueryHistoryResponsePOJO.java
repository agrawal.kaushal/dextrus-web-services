package com.rightdata.actuatorservice.POJOs;

import java.io.Serializable;
import java.util.List;

public class QueryHistoryResponsePOJO implements Serializable {

    private boolean flag;
    private List<QueryHistoryPOJO> data;
    private String failureCause;
    private String result;

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<QueryHistoryPOJO> getData() {
        return data;
    }

    public void setData(List<QueryHistoryPOJO> data) {
        this.data = data;
    }

    public String getFailureCause() {
        return failureCause;
    }

    public void setFailureCause(String failureCause) {
        this.failureCause = failureCause;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
