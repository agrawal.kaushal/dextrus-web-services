package com.rightdata.actuatorservice.Utils;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class SparkSessionUtil {
    public static SparkSession sparkSession;
//    public static String warehouseLocation = "s3a://dextrus.dec.eks/delta";
    private static SparkContext sparkContext;
    private static String deploymentMode;
    private static String masterEndPoint;
    public static String warehouseDir;
    private static String accessKey;
    private static String secretKey;
    private static String endPoint;
    private static String driverHost;
    private static String jars;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${deployment.mode}")
    public void setDeploymentMode(String deploymentMode) {
        SparkSessionUtil.deploymentMode = deploymentMode;
    }

    @Value("${spark.master}")
    public void setMasterEndPoint(String masterEndPoint) {
        SparkSessionUtil.masterEndPoint = masterEndPoint;
    }

    @Value("${spark.warehouse.dir}")
    public void setWarehouseDir(String warehouseDir) {
        SparkSessionUtil.warehouseDir = warehouseDir;
    }

    @Value("${spark.s3.accessKey}")
    public void setAccessKey(String accessKey) {
        SparkSessionUtil.accessKey = accessKey;
    }

    @Value("${spark.s3.secretKey}")
    public void setSecretKey(String secretKey) {
        SparkSessionUtil.secretKey = secretKey;
    }

    @Value("${spark.s3.endPoint}")
    public void setEndPoint(String endPoint) {
        SparkSessionUtil.endPoint = endPoint;
    }

    @Value("${spark.driver.host}")
    public void setDriverHost(String driverHost) {
        SparkSessionUtil.driverHost = driverHost;
    }

    @Value("${spark.jars}")
    public void setJars(String jars) {
        SparkSessionUtil.jars = jars;
    }

    public static void startSession() {
        File file = new File(warehouseDir);
        warehouseDir = file.getAbsolutePath();
//        try {
//            FileUtils.deleteDirectory(file);
//        } catch (IOException e) {
//            System.out.println("Cant find spark warehouse");
//        }
        setSparkConf();
        SparkSession session =  SparkSession.builder().sparkContext(sparkContext).enableHiveSupport().getOrCreate().newSession();
        System.out.println(session.conf().get("spark.sql.catalogImplementation"));
        sparkSession = session;
        System.out.println(warehouseDir);
        System.out.println("Creating spark session");

//        Dataset<Row> events = session.read().load("c://delta-warehouse/test-delta-4");
//        events.write().format("delta").save("c://delta-warehouse/events");
//        session.sql("CREATE TABLE events USING DELTA LOCATION 'c://delta-warehouse/events/'");

//        setSparkConf();
//        sparkSession =  SparkSession.builder().sparkContext(sparkContext).enableHiveSupport().getOrCreate().newSession();

//        sparkSession = SparkSession.builder().appName("Reading and writing to delta")
//                .master("local[*]")
////                .config("spark.sql.warehouse.dir", warehouseLocation)
////                .config("hive.metastore.warehouse.dir", warehouseLocation)
//                .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
//                .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
////                .config("spark.databricks.delta.preview.enabled", true)
////                .config("spark.databricks.delta.multiClusterWrites.enabled", true)
////                .config("spark.databricks.delta.logStore.crossCloud.fatal", true)
////                .config("spark.databricks.acl.sqlOnly", true)
////                .config("spark.sql.sources.commitProtocolClass", "com.databricks.sql.transaction.directory.DirectoryAtomicCommitProtocol")
//                .getOrCreate().newSession();

//        sparkSession = SparkSession.builder().appName("Reading and writing to delta")
//                .master("k8s://http://localhost:8001")
//                .config("spark.kubernetes.container.image", "290384616740.dkr.ecr.us-east-1.amazonaws.com/rightdata/spark:v3.0.1-hive-s3")
//                .config("spark.sql.warehouse.dir", "s3a://dextrus.dec.eks/user/hive/warehouse")
//                .config("spark.hadoop.fs.s3a.access.key","AKIAUHHCFTUSLFIJTGA7")
//                .config("spark.hadoop.fs.s3a.secret.key", "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")
//                .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
//                .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
//                .getOrCreate().newSession();
        System.out.println("spark session created");
        System.out.println(sparkSession);
    }

    private static void setSparkConf() {
        System.out.println("in config");
        /*System.setProperty("java.security.krb5.conf", env.getProperty("security.krb5_conf"));
        Configuration conf = new Configuration();
        conf.set("hadoop.security.authentication", env.getProperty("hadoop.security_authentication"));
        conf.set("hadoop.security.authorization", env.getProperty("hadoop.security_authorization"));
        conf.set("fs.defaultFS", env.getProperty("hadoop.fs_defaultFS"));
        conf.set("hadoop.rpc.protection", env.getProperty("hadoop.rpc_protection"));
        conf.set("dfs.namenode.kerberos.principal",env.getProperty("hadoop.dfs_namenode_kerberos_principal"));
        conf.set("spark.yarn.principal", env.getProperty("spark.yarn_principal"));
        UserGroupInformation.setConfiguration(conf);
        try {
            UserGroupInformation.loginUserFromKeytab(env.getProperty("spark.yarn_principal"),
                    env.getProperty("spark.keytab_path"));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
//        System.setProperty()
//        System.setProperty("hive.metastore.uris", "thrift://0.0.0.0:9083");
//        SparkConf sparkConf = new SparkConf().setAppName("dbexplorer").setMaster("local[5]")
//                .set("spark.driver.extraJavaOptions", "-Dderby.system.home=/tmp/derby");=
        SparkConf sparkConf = null;
        System.out.println(deploymentMode + masterEndPoint + driverHost + jars);
        if (deploymentMode.equalsIgnoreCase("standalone")) {
            sparkConf = new SparkConf().setAppName("Dextrusdbexplorer").setMaster("local[*]")
                    .set("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
                    .set("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
                    .set("spark.sql.warehouse.dir", warehouseDir);  //configurable
//                    .set("spark.sql.warehouse.dir", "c://delta-warehouse1")   //configurable
//                    .set("hive.metastore.warehouse.dir", "c://delta-warehouse2");  //configurable;
//                    .set("datanucleus.schema.autoCreateTables", String.valueOf(true));  //configurable;
        } else {
            sparkConf = new SparkConf().setAppName("Dextrusdbexplorer")
                    .setMaster("k8s://" + masterEndPoint)  //configurable
                    .set("spark.kubernetes.container.image", "290384616740.dkr.ecr.us-east-1.amazonaws.com/rightdata/spark:v3.0.1-hive-s3")
//                //.set("spark.driver.extraClassPath", env.getProperty("spark.driver_extraClassPath"))
//                //.set("spark.kubernetes.kerberos.enabled", env.getProperty("spark.kubernetes_kerberos_enabled"))
                    .set("spark.kubernetes.executor.deleteOnTermination", "false")
                    .set("spark.driver.host", driverHost) // //configurable dextrus-web-services-headless
                    .set("spark.driver.port", "44103")
                    .set("spark.blockManager.port", "61023")
                    .set("spark.port.maxRetries", "20")
//                //.set("spark.kerberos.access.hadoopFileSystems", env.getProperty("spark.kerberos_access_hadoopFileSystems"))
//                //.set("spark.kerberos.keytab", env.getProperty("spark.kerberos_keytab"))
//                //.set("spark.kerberos.principal",env.getProperty("spark.kerberos_principal"))
//                //.set("spark.kubernetes.kerberos.krb5.path",env.getProperty("spark.kubernetes_kerberos_krb5_path"))
//                //.set("spark.driver.memory", env.getProperty("spark.driver_memory"))
//                //.set("spark.executor.memory", env.getProperty("spark.executor_memory"))
//                //.set("spark.driver.memoryOverhead",env.getProperty("spark.driver_memoryOverhead"))
//                //.set("spark.executor.memoryOverhead",env.getProperty("spark.executor_memoryOverhead"))
//                //.set("spark.dynamicAllocation.enabled", env.getProperty("spark.dynamicAllocation_enabled"))
//                //.set("spark.dynamicAllocation.shuffleTracking.enabled", env.getProperty("spark.dynamicAllocation_shuffleTracking_enabled"))
//                //.set("spark.dynamicAllocation.executorIdleTimeout", env.getProperty("spark.dynamicAllocation_executorIdleTimeout"))
//                //.set("spark.dynamicAllocation.schedulerBacklogTimeout", env.getProperty("spark.dynamicAllocation_schedulerBacklogTimeout"))
//                //.set("spark.executor.cores",env.getProperty("spark.executor_cores"))
//                //.set("spark.dynamicAllocation.minExecutors",env.getProperty("spark.dynamicAllocation_minExecutors"))
//                //.set("spark.dynamicAllocation.maxExecutors",env.getProperty("spark.dynamicAllocation_maxExecutors"))
                    .set("spark.sql.warehouse.dir", warehouseDir)  //configurable
//                //.set("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3.S3FileSystem")
                    .set("spark.hadoop.fs.s3a.access.key", accessKey)  //configurable
                    .set("spark.hadoop.fs.s3a.secret.key", secretKey) //configurable
                    .set("hive.metastore.warehouse.dir", warehouseDir)
                    .set("spark.hadoop.fs.s3a.endpoint", endPoint)  //configurable
////                .set("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0")
                    .set("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
                    .set("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
                    .set("spark.jars", jars);
//        .set("spark.jars", "s3a://dextrus.dec.eks/snowflake-jdbc-3.12.16.jar,s3a://dextrus.dec.eks/spark-snowflake_2.12-2.8.3-spark_3.0.jar,s3a://dextrus.dec.eks/spark-redshift_2.11-3.0.0-preview1.jar,s3a://dextrus.dec.eks/mysql-connector-java-8.0.20.jar");  //configurable
        }
        JavaSparkContext ctx = new JavaSparkContext(sparkConf);
        sparkContext = ctx.sc();
    }
}
