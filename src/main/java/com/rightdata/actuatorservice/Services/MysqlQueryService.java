package com.rightdata.actuatorservice.Services;

import com.rightdata.actuatorservice.POJOs.QueryServicePOJO;

import java.sql.Connection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MysqlQueryService {

    public String handleQuery(QueryServicePOJO pojo, String sourceTableOrQuery) {
        int limit = pojo.getSource().getPreviewCount();
        int offset = pojo.getSource().getSkipRows();
        String connectionJDBCURL = pojo.getSource().getConnectionInfo().getConnectionJDBCURL();
        sourceTableOrQuery = setLimitAndOffset(connectionJDBCURL, sourceTableOrQuery, limit, offset);

        return sourceTableOrQuery;
    }

    private String setLimitAndOffset(String connectionJDBCURL, String sourceTableOrQuery, int limit, int offset) {
        if (sourceTableOrQuery.toUpperCase().startsWith("SELECT ")) {
            if (sourceTableOrQuery.endsWith(";")){
                sourceTableOrQuery = sourceTableOrQuery.substring(0, sourceTableOrQuery.length() - 1);
            }
            String[] queries = sourceTableOrQuery.split("\\)");
            String lastQuery = queries[queries.length - 1];
            String limitPatternString = "limit";
            Pattern limitPattern = Pattern.compile(limitPatternString, Pattern.CASE_INSENSITIVE);
            Matcher limitMatcher = limitPattern.matcher(lastQuery);
            String offsetPatternString = "offset";
            Pattern offsetPattern = Pattern.compile(offsetPatternString, Pattern.CASE_INSENSITIVE);
            Matcher offsetMatcher = offsetPattern.matcher(lastQuery);

            if (!limitMatcher.find()) {
                sourceTableOrQuery = sourceTableOrQuery + " LIMIT " + limit;
            }
            if (!offsetMatcher.find()) {
                sourceTableOrQuery = sourceTableOrQuery + " OFFSET " + offset;
            }
        }
        return sourceTableOrQuery;
    }
}
