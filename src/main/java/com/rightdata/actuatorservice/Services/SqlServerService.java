package com.rightdata.actuatorservice.Services;

import com.rightdata.actuatorservice.POJOs.QueryServicePOJO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlServerService {

    public String handleQuery(QueryServicePOJO pojo, String sourceTableOrQuery) {
        int limit = pojo.getSource().getPreviewCount();
        int offset = pojo.getSource().getSkipRows();
        String connectionJDBCURL = pojo.getSource().getConnectionInfo().getConnectionJDBCURL();
        sourceTableOrQuery = setLimitAndOffset(connectionJDBCURL, sourceTableOrQuery, limit, offset);

        return sourceTableOrQuery;
    }

    private String setLimitAndOffset(String connectionJDBCURL, String sourceTableOrQuery, int limit, int offset) {
        if (sourceTableOrQuery.toUpperCase().startsWith("SELECT ")) {
            if (sourceTableOrQuery.endsWith(";")){
                sourceTableOrQuery = sourceTableOrQuery.substring(0, sourceTableOrQuery.length() - 1);
            }
            String[] queries = sourceTableOrQuery.split("\\)");
            String lastQuery = queries[queries.length - 1];
            String orderPatternString = "order\\s+by";
            Pattern orderPattern = Pattern.compile(orderPatternString, Pattern.CASE_INSENSITIVE);
            Matcher orderMatcher = orderPattern.matcher(lastQuery);
            String limitPatternString = "limit";
            Pattern limitPattern = Pattern.compile(limitPatternString, Pattern.CASE_INSENSITIVE);
            Matcher limitMatcher = limitPattern.matcher(lastQuery);
            String offsetPatternString = "offset";
            Pattern offsetPattern = Pattern.compile(offsetPatternString, Pattern.CASE_INSENSITIVE);
            Matcher offsetMatcher = offsetPattern.matcher(lastQuery);
//            if (isMSSQLServerVer2012OrLater(conn)) {
                if (!orderMatcher.find()) {
                    sourceTableOrQuery = sourceTableOrQuery + " ORDER BY (SELECT NULL) ";
                }
                if (!offsetMatcher.find()) {
                    sourceTableOrQuery = sourceTableOrQuery + " OFFSET " + offset + " ROWS ";
                }
                if (!limitMatcher.find()) {
                    sourceTableOrQuery = sourceTableOrQuery + " FETCH NEXT " + limit + " ROWS ONLY ";
                }
//            }
        }
        return sourceTableOrQuery;
    }

    public boolean isMSSQLServerVer2012OrLater(Connection con) {
        Statement statement;
        ResultSet rs;
        try {
            statement = con.createStatement();
            rs = statement.executeQuery("SELECT" +
                    "  CASE " +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '8%' THEN 'SQL2000'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '9%' THEN 'SQL2005'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '10.0%' THEN 'SQL2008'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '10.5%' THEN 'SQL2008 R2'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '11%' THEN 'SQL2012'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '12%' THEN 'SQL2014'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '13%' THEN 'SQL2016'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '14%' THEN 'SQL2017'" +
                    "     ELSE 'unknown'" +
                    "  END AS MajorVersion");
            if (rs.next()) {
                String version = rs.getString("MajorVersion");
//                log.info("MSSQL Version: " + version);
                return version.equalsIgnoreCase("SQL2012") || version.equalsIgnoreCase("SQL2014") || version.equalsIgnoreCase("SQL2016") || version.equalsIgnoreCase("SQL2017");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
