package com.rightdata.actuatorservice.Services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.rightdata.actuatorservice.ActuatorserviceApplication;
import com.rightdata.actuatorservice.POJOs.Navigator.*;
import com.rightdata.actuatorservice.POJOs.QueryServicePOJO;
import com.rightdata.actuatorservice.POJOs.ResponsePOJO;
import com.rightdata.actuatorservice.Utils.SparkSessionUtil;
import io.delta.tables.*;
import org.apache.hadoop.mapred.Mapper;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.logging.Logger;

@Service
public class DeltaService {

    private static final Logger LOGGER = Logger.getLogger(String.valueOf(DeltaService.class));

    private final static String STORE_FILE_PATH = "DeltaWareHouse/SalaryDeltaTable";
    private final static String Delta_STORE_FILE_PATH = "DeltaWareHouse/";

    @Autowired
    public QueryHistoryService queryHistoryService;

    public ResponseEntity<ResponsePOJO> runQuery(QueryServicePOJO queryServiceString) {
        HttpHeaders responseHeaders = new HttpHeaders();
        ResponsePOJO responsePOJO = new ResponsePOJO();

        String purpose = queryServiceString.getSource().getPurpose();

        if (purpose.equalsIgnoreCase("getVersionedData")) {
            SparkSession sparkSession = SparkSessionUtil.sparkSession;
            return getVersionedData(sparkSession, queryServiceString);
        }
        return runDeltaQuery(queryServiceString);
    }

        public ResponseEntity<ResponsePOJO> runDeltaQuery(QueryServicePOJO queryServiceString) {
//        SparkSessionUtil.startSession();
        HttpHeaders responseHeaders = new HttpHeaders();
        ResponsePOJO responsePOJO = new ResponsePOJO();
        System.out.println(SparkSessionUtil.warehouseDir);
        SparkSession sparkSession = SparkSessionUtil.sparkSession;
        long startTime = System.nanoTime();
        long endTime = System.nanoTime();

        try {
            startTime = System.nanoTime();
            Dataset<Row> result = sparkSession.sql(queryServiceString.getSource().getSourceInfo().getSourceTableOrQuery());
            endTime = System.nanoTime();
//            Dataset<String> jsonDataset = result.toJSON();
            List<String> stringDataset = result.toJSON().collectAsList();
            String columns = getColumnsForData(result.schema().fields());

            if (queryServiceString.getSource().getSourceInfo().getSourceTableOrQuery().toUpperCase().startsWith("SELECT ")) {
                responsePOJO.setSelectQuery(true);
            }
            responsePOJO.setFlag(true);
            responsePOJO.setResult("Success");
            responsePOJO.setExecutionTime(endTime - startTime);
            responsePOJO.setData(stringDataset.toString());
            responsePOJO.setColumns(columns);
            responsePOJO.setFetchNext(false);
            long finalEndTime1 = endTime;
            long finalStartTime1 = startTime;
            new Thread(() -> {
                queryHistoryService.saveQuery(queryServiceString, queryServiceString.getSource().getSourceInfo().getSourceTableOrQuery(), finalEndTime1 - finalStartTime1, "success", true);
            }).start();
            return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            responsePOJO.setFlag(false);
            responsePOJO.setResult(e.getMessage());
            if (e.getCause() != null) {
                responsePOJO.setFailureCause(e.getCause().toString());
            }
            long finalEndTime = endTime;
            long finalStartTime = startTime;
            new Thread(() -> {
                queryHistoryService.saveQuery(queryServiceString, queryServiceString.getSource().getSourceInfo().getSourceTableOrQuery(), finalEndTime - finalStartTime, e.getLocalizedMessage(), false);
            }).start();
            LOGGER.info(e.getMessage());
            return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);
        } finally {
//            SparkSessionUtil.closeSession();
        }
    }

    public ResponseEntity<NavigationResponsePOJO> getNavigatorList(QueryServicePOJO queryServiceString) {
        HttpHeaders responseHeaders = new HttpHeaders();
        NavigationResponsePOJO response = new NavigationResponsePOJO();

        SparkSession sparkSession = SparkSessionUtil.sparkSession;
        try {
            if(queryServiceString.getSource().getPurpose().equalsIgnoreCase("catalogList")){
                return getDatabasesList(sparkSession);
            } else if(queryServiceString.getSource().getPurpose().equalsIgnoreCase("tablesList")) {
                return getTablesList(sparkSession, queryServiceString);
            } else if(queryServiceString.getSource().getPurpose().equalsIgnoreCase("columnsList")) {
                return getColumnsList(sparkSession, queryServiceString);
            } else if(queryServiceString.getSource().getPurpose().equalsIgnoreCase("versionsList")) {
                return getVersionsList(sparkSession, queryServiceString);
            } else if(queryServiceString.getSource().getPurpose().equalsIgnoreCase("versionedColumnsList")) {
                return getVersionedColumnsList(sparkSession, queryServiceString);
            } else if(queryServiceString.getSource().getPurpose().equalsIgnoreCase("search")) {
                return getSearchTablesList(sparkSession, queryServiceString);
            }

//            Dataset<Row> result = sparkSession.sql(queryServiceString.getSource().getSourceInfo().getSourceTableOrQuery());
//            Dataset<String> jsonDataset = result.toJSON();
//            List<String> stringDataset = jsonDataset.collectAsList();
//            String columns = getColumnsForData(result.schema().fields());
//            response.setData(stringDataset.toString());

            response.setStatus(true);
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            response.setStatus(false);
            response.setTechnicalMessage(e.getMessage());
            if (e.getCause() != null) {
                response.setUserMessage(e.getCause().toString());
            }
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        }
    }

    public ResponseEntity<NavigationResponsePOJO> getDatabasesList(SparkSession sparkSession) throws JSONException {
        Iterator<String> show_databases = sparkSession.sql("SHOW Databases")
                .toJSON().toLocalIterator();
        HttpHeaders responseHeaders = new HttpHeaders();
        NavigationResponsePOJO response = new NavigationResponsePOJO();

        try {
            List<CatalogsPOJO> catalogsPOJOS = new ArrayList<>();
            while (show_databases.hasNext()) {
                String databaseName = new JSONObject(show_databases.next()).getString("namespace");
                if (!databaseName.equalsIgnoreCase("default")) {
                    CatalogsPOJO catalogsPOJO = new CatalogsPOJO();
                    catalogsPOJO.CATALOG_NAME = databaseName;
                    catalogsPOJOS.add(catalogsPOJO);
                }
            }
            response.catalogs = catalogsPOJOS;
            response.setStatus(true);
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            response.setStatus(false);
            response.setTechnicalMessage(e.getMessage());
            if (e.getCause() != null) {
                response.setUserMessage(e.getCause().toString());
            }
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        }
    }

    public ResponseEntity<NavigationResponsePOJO> getTablesList(SparkSession sparkSession, QueryServicePOJO pojo) throws JSONException {
        String query = "show tables ";
        if (pojo.getSource().getConnectionInfo().getDb() != null) {
            query += " from " + pojo.getSource().getConnectionInfo().getDb();
        }
        query = query.replaceAll(";", "");
        List<Row> tablesList = sparkSession.sql(query).collectAsList();
        HttpHeaders responseHeaders = new HttpHeaders();
        NavigationResponsePOJO response = new NavigationResponsePOJO();

        try {
            List<TablesPOJO> tablesPOJOS = new ArrayList<>();
            for (Row table : tablesList) {
                TablesPOJO tablesPOJO = new TablesPOJO();
                tablesPOJO.tableName = table.get(1).toString();
//                tablesPOJO.provider = new JSONObject(tablesList.next()).getString("information");
                tablesPOJO.isDelta = true;
                tablesPOJOS.add(tablesPOJO);
            }
            response.tables = tablesPOJOS;
            response.setStatus(true);
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            response.setStatus(false);
            response.setTechnicalMessage(e.getMessage());
            if (e.getCause() != null) {
                response.setUserMessage(e.getCause().toString());
            }
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        }
    }

    public ResponseEntity<NavigationResponsePOJO> getVersionsList(SparkSession sparkSession, QueryServicePOJO pojo) throws JSONException {
        String db = pojo.getSource().getConnectionInfo().getDb();
        String table = pojo.getSource().getSourceInfo().getSourceTableOrQuery();
        String warehouseLocation = SparkSessionUtil.warehouseDir;
        if (db != null) {
            warehouseLocation += "/" + db + ".db";
        }
        if (table != null) {
            warehouseLocation += "/" + table;
        }

        DeltaTable deltaTable = DeltaTable.forPath(sparkSession, warehouseLocation);

        Iterator<Row> versionsList = deltaTable.history().collectAsList().iterator();       // get the full history of the table
        HttpHeaders responseHeaders = new HttpHeaders();
        NavigationResponsePOJO response = new NavigationResponsePOJO();

        try {
            List<VersionsPOJO> versionsPOJOS = new ArrayList<>();
            while (versionsList.hasNext()) {
                VersionsPOJO versionsPOJO = new VersionsPOJO();
                Row version = versionsList.next();
                versionsPOJO.versionName = version.get(0).toString();
                versionsPOJO.timestamp = version.get(1).toString();
                versionsPOJOS.add(versionsPOJO);
            }
            response.versions = versionsPOJOS;
            response.setStatus(true);
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            response.setStatus(false);
            response.setTechnicalMessage(e.getMessage());
            if (e.getCause() != null) {
                response.setUserMessage(e.getCause().toString());
            }
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        }
    }

    public ResponseEntity<NavigationResponsePOJO> getColumnsList(SparkSession sparkSession, QueryServicePOJO queryServicePOJO) throws JSONException {
        String db = queryServicePOJO.getSource().getConnectionInfo().getDb();
        String table = queryServicePOJO.getSource().getSourceInfo().getSourceTableOrQuery();
//        String version = queryServicePOJO.getSource().getSourceInfo().getVersion();
        String query = "SELECT * FROM ";
        if (db != null) {
            query += db + ".";
        }
        query += table;
//        if (version != null) {
//            query += " VERSION AS OF " + version;
//        }
        query +=  " limit 1";
        query = query.replaceAll(";", "");
        Dataset<Row> result = sparkSession.sql(query);
        HttpHeaders responseHeaders = new HttpHeaders();
        NavigationResponsePOJO response = new NavigationResponsePOJO();

        try {
            List<ColumnsPOJO> columnsPOJOS = new ArrayList<>();
            for (StructField field : result.schema().fields()) {
                ColumnsPOJO columnsPOJO = new ColumnsPOJO();
                columnsPOJO.name = field.name();
                columnsPOJO.systemDataType = field.dataType().typeName();
                //jsonObject.put("length", JSONObject.NULL);
                //jsonObject.put("precision", JSONObject.NULL);
                columnsPOJOS.add(columnsPOJO);
            }
            response.columns = columnsPOJOS;
            response.setStatus(true);
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            response.setStatus(false);
            response.setTechnicalMessage(e.getMessage());
            if (e.getCause() != null) {
                response.setUserMessage(e.getCause().toString());
            }
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        }
    }

    public ResponseEntity<NavigationResponsePOJO> getSearchTablesList(SparkSession sparkSession, QueryServicePOJO pojo) throws JSONException {
        String searchString = pojo.getSource().getSearchPattern().replaceAll("\\*", "").replaceAll(" ", "");
        HttpHeaders responseHeaders = new HttpHeaders();
        NavigationResponsePOJO response = new NavigationResponsePOJO();
        List<SearchResultPOJO> searchResultPOJOS = new ArrayList<>();

        try {
            Iterator<String> show_databases = sparkSession.sql("SHOW Databases")
                    .toJSON().toLocalIterator();

            while (show_databases.hasNext()) {
                String databaseName = new JSONObject(show_databases.next()).getString("namespace");
                if (!databaseName.equalsIgnoreCase("default")) {
                    String query = "show table extended from " + databaseName;
                    query += "  like  '*" + searchString + "*'";
                    query = query.replaceAll(";", "");
                    List<Row> tablesList = sparkSession.sql(query).collectAsList();


                    for (Row table : tablesList) {
                        SearchResultPOJO searchResultPOJO = new SearchResultPOJO();
                        searchResultPOJO.CATALOG_NAME = table.get(0).toString();
                        searchResultPOJO.tableName = table.get(1).toString();
        //                tablesPOJO.provider = new JSONObject(tablesList.next()).getString("information");
                        searchResultPOJO.isDelta = true;
                        searchResultPOJOS.add(searchResultPOJO);
                    }
                }
            }
            response.searchResults = searchResultPOJOS;
            response.setStatus(true);
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            response.setStatus(false);
            response.setTechnicalMessage(e.getMessage());
            if (e.getCause() != null) {
                response.setUserMessage(e.getCause().toString());
            }
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        }
    }

    public ResponseEntity<NavigationResponsePOJO> getVersionedColumnsList(SparkSession sparkSession, QueryServicePOJO queryServicePOJO) {
        HttpHeaders responseHeaders = new HttpHeaders();
        NavigationResponsePOJO response = new NavigationResponsePOJO();

        String warehouseLocation = SparkSessionUtil.warehouseDir;
        sparkSession = SparkSessionUtil.sparkSession;
        String path = warehouseLocation;
        try {
            String db = queryServicePOJO.getSource().getConnectionInfo().getDb();
            String table = queryServicePOJO.getSource().getSourceInfo().getSourceTableOrQuery();
            String version = queryServicePOJO.getSource().getSourceInfo().getVersion();


            if (db != null) {
                warehouseLocation += "/" + db + ".db";
            }
            if (table != null) {
                warehouseLocation += "/" + table;
            }
            Dataset<Row> result = sparkSession.read().format("delta").option("versionAsOf", version).load( warehouseLocation );
//            Dataset<String> jsonDataset = result.toJSON();
//            List<String> stringDataset = jsonDataset.collectAsList();
//            String columns = getColumnsForData(result.schema().fields());
            try {
                List<ColumnsPOJO> columnsPOJOS = new ArrayList<>();
                for (StructField field : result.schema().fields()) {
                    ColumnsPOJO columnsPOJO = new ColumnsPOJO();
                    columnsPOJO.name = field.name();
                    columnsPOJO.systemDataType = field.dataType().typeName();
                    //jsonObject.put("length", JSONObject.NULL);
                    //jsonObject.put("precision", JSONObject.NULL);
                    columnsPOJOS.add(columnsPOJO);
                }
                response.columns = columnsPOJOS;
                response.setStatus(true);
                return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
            } catch (Exception e) {
                response.setStatus(false);
                response.setTechnicalMessage(e.getMessage());
                if (e.getCause() != null) {
                    response.setUserMessage(e.getCause().toString());
                }
                return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
            }
        } catch (Exception e) {
            response.setStatus(false);
            response.setTechnicalMessage(e.getMessage());
            if (e.getCause() != null) {
                response.setUserMessage(e.getCause().toString());
            }
            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        }
    }

    public ResponseEntity<ResponsePOJO> getVersionedData(SparkSession sparkSession, QueryServicePOJO queryServicePOJO) {
        HttpHeaders responseHeaders = new HttpHeaders();
        ResponsePOJO response = new ResponsePOJO();

        String warehouseLocation = SparkSessionUtil.warehouseDir;
        sparkSession = SparkSessionUtil.sparkSession;
        String path = warehouseLocation;
        try {
            String db = queryServicePOJO.getSource().getConnectionInfo().getDb();
            String table = queryServicePOJO.getSource().getSourceInfo().getSourceTableOrQuery();
            String version = queryServicePOJO.getSource().getSourceInfo().getVersion();


            if (db != null) {
                warehouseLocation += "/" + db + ".db";
            }
            if (table != null) {
                warehouseLocation += "/" + table;
            }
            long startTime = System.nanoTime();
            long endTime = System.nanoTime();
            Dataset<Row> result = sparkSession.read().format("delta").option("versionAsOf", version).load( warehouseLocation );
            endTime = System.nanoTime();

//            Dataset<String> jsonDataset = result.toJSON();
//            List<String> stringDataset = jsonDataset.collectAsList();
//            String columns = getColumnsForData(result.schema().fields());
            try {
                List<String> collectionData = result.toJSON().collectAsList();
//            Dataset<String> jsonDataset = dataframe.toJSON();
                String stringDataset = collectionData.toString();
                int count = collectionData.size();
                String columns = getColumnsForData(result.schema().fields());

                response.setFlag(true);
                response.setSelectQuery(true);
                response.setExecutionTime(endTime - startTime);
                response.setData(stringDataset);
                response.setColumns(columns);
                response.setCount(count);

                return new ResponseEntity<ResponsePOJO>(response, responseHeaders, HttpStatus.OK);
            } catch (Exception e) {
                response.setFlag(false);
                response.setResult(e.getMessage());
                if (e.getCause() != null) {
                    response.setFailureCause(e.getCause().toString());
                }
                return new ResponseEntity<ResponsePOJO>(response, responseHeaders, HttpStatus.OK);
            }
        } catch (Exception e) {
            response.setFlag(false);
            response.setResult(e.getMessage());
            if (e.getCause() != null) {
                response.setFailureCause(e.getCause().toString());
            }
            return new ResponseEntity<ResponsePOJO>(response, responseHeaders, HttpStatus.OK);
        }
    }

//    public ResponseEntity<ResponsePOJO> getVersionHistoryData(SparkSession sparkSession, QueryServicePOJO queryServicePOJO) {
//        HttpHeaders responseHeaders = new HttpHeaders();
//        ResponsePOJO response = new ResponsePOJO();
//
//        String warehouseLocation = SparkSessionUtil.warehouseLocation;
//        sparkSession = SparkSessionUtil.sparkSession;
//        String path = warehouseLocation;
//        try {
//            String db = queryServicePOJO.getSource().getConnectionInfo().getDb();
//            String table = queryServicePOJO.getSource().getSourceInfo().getSourceTableOrQuery();
//            String version = queryServicePOJO.getSource().getSourceInfo().getVersion();
//
//
//            if (db != null) {
//                warehouseLocation += "/" + db + ".db";
//            }
//            if (table != null) {
//                warehouseLocation += "/" + table;
//            }
//            Dataset<Row> result = sparkSession.read().format("delta").option("versionAsOf", version).load( warehouseLocation );
////            Dataset<String> jsonDataset = result.toJSON();
////            List<String> stringDataset = jsonDataset.collectAsList();
////            String columns = getColumnsForData(result.schema().fields());
//            try {
//                List<ColumnsPOJO> columnsPOJOS = new ArrayList<>();
//                for (StructField field : result.toJSON().collectAsList()) {
//                    ColumnsPOJO columnsPOJO = new ColumnsPOJO();
//                    columnsPOJO.name = field.name();
//                    columnsPOJO.systemDataType = field.dataType().typeName();
//                    //jsonObject.put("length", JSONObject.NULL);
//                    //jsonObject.put("precision", JSONObject.NULL);
//                    columnsPOJOS.add(columnsPOJO);
//                }
//                response.columns = columnsPOJOS;
//                response.setStatus(true);
//                return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
//            } catch (Exception e) {
//                response.setStatus(false);
//                response.setTechnicalMessage(e.getMessage());
//                if (e.getCause() != null) {
//                    response.setUserMessage(e.getCause().toString());
//                }
//                return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
//            }
//        } catch (Exception e) {
//            response.setStatus(false);
//            response.setTechnicalMessage(e.getMessage());
//            if (e.getCause() != null) {
//                response.setUserMessage(e.getCause().toString());
//            }
//            return new ResponseEntity<NavigationResponsePOJO>(response, responseHeaders, HttpStatus.OK);
//        }
//    }

//    public String getColumnNames(Dataset<String> jsonDataset) {
//        JSONArray array = new JSONArray();
//        if(jsonDataset.collectAsList().size() > 0) {
//            JSONObject object = null;
//            try {
//                object = new JSONObject(jsonDataset.collectAsList().get(0));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            array.put(object.keySet().toString());
//        }
//        return array.toString();
//    }

    private String getColumnsForData(StructField[] fields) {
        JSONArray jsonArray = new JSONArray();
        try {
            for (StructField field : fields) {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("columnName", field.name());
                jsonObject.put("dataType", field.dataType().typeName());
                //jsonObject.put("length", JSONObject.NULL);
                //jsonObject.put("precision", JSONObject.NULL);
                jsonArray.put(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return jsonArray.toString();
    }

    public void runQueryOnDelta(QueryServicePOJO queryServiceString) throws JSONException, IOException {
//        Logger.getLogger("org.apache").setLevel(Level.WARN);
//        System.out.println(warehouseLocation);

        SparkSession sparkSession = SparkSessionUtil.sparkSession;
//        DeltaServiceUtil deltaServiceUtil = new DeltaServiceUtil();

        // Create a table
//        sparkSession.sql("CREATE TABLE tableName2(id LONG) USING delta");
//        sparkSession.sql("INSERT INTO tableName2 VALUES 0, 1, 2, 3, 4;");

        // Read table
        sparkSession.sql(queryServiceString.getSource().getSourceInfo().getSourceTableOrQuery()).show();

//        DeltaTable deltaTable = DeltaTable.forPath(sparkSession, warehouseLocation  + "\\customer");

//        DataFrame fullHistoryDF = deltaTable.history();       // get the full history of the table

//        DataFrame lastOperationDF = deltaTable.history(1);

//        sparkSession.sql("CREATE DATABASE IF NOT EXISTS dextrus_spark_managed_db " +
//                "COMMENT 'Database for dextrus spark managed table';");
//        sparkSession.sql("CREATE DATABASE IF NOT EXISTS dextrus_delta_db " +
//                "COMMENT 'Database for dextrus delta tables ';");



//        THIS METHOD IS FOR GETTING THE LIST OF DATABASES.
//        JSONObject databasesList = deltaServiceUtil.getDatabasesList(sparkSession);
//        System.out.println(databasesList);
//-----------------------------------------------------------------------------------------------------------

//        sparkSession.sql("CREATE TABLE customer(c_id Long, c_name String, c_city String) USING DELTA");
//        sparkSession.sql("INSERT INTO  customer VALUES(1, 'John', 'New York'), (2, 'Shawn', 'California')");
//        sparkSession.sql("INSERT INTO  customer VALUES(1, 'John', 'New York'), (1, 'Shawn', 'California')");
//        sparkSession.sql("SELECT * FROM customer").show();


//        Dataset<Row> df = sparkSession.read().format("delta").option("versionAsOf", 2).load( warehouseLocation + "\\customer");
//        df.show();

        //THIS METHOD IS FOR GETTING RESULT OF DQL STATMENT
//        sparkSession.sql("show tables ");
//        sparkSession.sql("show databases ");
//  sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(1, 'shampoo', 'Delhi')");
//  sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(2, 'shampoo', 'Delhi')");
//  sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(3, 'shampoo', 'Noida')");
//  sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(4, 'shampoo', 'Delhi')");
//  sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(5, 'shampoo', 'Noida')");

//  String dql = "Select * from dextrus_spark_managed_db.customer1";
//
//  JSONObject resultForDQL = deltaServiceUtil.getResultForDQL(sparkSession, dql);
//  System.out.println(resultForDQL);

//-----------------------------------------------------------------------------------------------------------------------------------

        // THIS METHOD IS FOR GETTING PARQUET FILE AND SIZE OF SPARK AND DELTA TABLE.
 /*   sparkSession.sql("CREATE TABLE dextrus_spark_managed_db.customer(c_id LONG, product_name String, city String) USING parquet");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(1, 'shampoo', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(2, 'shampoo', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(3, 'shampoo', 'Noida')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(4, 'shampoo', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(5, 'shampoo', 'Noida')");

    JSONArray finder = deltaHistory.getParquetFilesAndSize(warehouseLocation + "/dextrus_spark_managed_db.db/customer", sparkSession);
    System.out.println(finder.toString());*/

//-------------------------------------------------------------------------------------------------------------------------
        // tHIS METHOD IS FOR GEETING LOGS FOR DDL AND DML STATEMENTS
  /* String query = "CREATE TABLE dextrus_spark_managed_db.customer(c_id Long, product_name String, city String) USING parquet";

    String query1 = "Drop TABLE dextrus_spark_managed_db.customer";

    JSONObject create = deltaHistory.getLogForSparkTable(sparkSession, query, "Create");
    System.out.println(create);

    sparkSession.sql("CREATE TABLE dextrus_delta_db.customer(c_id LONG, product_name String, city String) USING Delta");
    String query2 = "CREATE TABLE dextrus_delta_db.customer(c_id LONG, product_name String, city String) USING DELTA";

    String query3 = "Drop TABLE dextrus_spark_managed_db.customer";

    JSONObject create1 = deltaHistory.getLogForDeltaTableDDL(sparkSession, query, "Create");
    System.out.println(create1);*/
//------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
        //THIS METHOD IS FOR GEETING THE LIST OF DELTA TABLES
/*
    sparkSession.sql("CREATE TABLE dextrus_delta_db.customer(c_id LONG, c_name String) USING delta");
    sparkSession.sql("CREATE TABLE dextrus_delta_db.employee(e_id LONG, e_name String, salary Double) USING delta");

    sparkSession.sql("INSERT INTO dextrus_delta_db.customer VALUES(1, 'abc')");
    sparkSession.sql("INSERT INTO dextrus_delta_db.customer VALUES(2, 'xyz')");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(11, 'Jhon', 12465.23)");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(12, 'Tucker', 22135.03)");

    JSONObject deltaTableList = deltaHistory.getDeltaTableList(sparkSession);
    System.out.println(".............Delta tables..............." + "\n" + deltaTableList);
*/

//-----------------------------------------------------------------------------------------------------------------------

/*
    sparkSession.sql("CREATE TABLE dextrus_spark_managed_db.customerPurchase(c_id LONG, product_name String) " +
            "ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TextFile");
    sparkSession.sql("CREATE TABLE dextrus_spark_managed_db.customerBill(p_id LONG, price Double) " +
            "ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TextFile");

    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customerPurchase VALUES(1, 'shampoo')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customerPurchase VALUES(2, 'toothpaste')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customerBill VALUES(11, 15)");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customerBill VALUES(12, 17.05)");

    JSONObject sparkTableList = deltaHistory.getSparkTableList(sparkSession);
    System.out.println(".............spark tables..............." + "\n" + sparkTableList);
*/

//---------------------------------------------------------------------------------------------------------------------------

        //this two method demonstrate to get the partition info of spark table and thenget the partion deatil of table.
/*     sparkSession.sql("CREATE TABLE dextrus_spark_managed_db.customer(c_id LONG, product_name String, city String)" +
            "  USING parquet PARTITIONED BY (product_name,city)");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(11, 'shampoo', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(12, 'shampoo', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(13, 'toothpaste', 'Noida')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(14, 'shampoo', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(15, 'toothpaste', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(16, 'shampoo', 'Noida')");
    sparkSession.sql("INSERT INTO dextrus_spark_managed_db.customer VALUES(17, 'toothpaste', 'Noida')");

    JSONObject customerPartitions = deltaHistory.getSparkTablePartitionInfo(sparkSession, "customer");
    System.out.println("............. spark Customer table Partitions info..............." + "\n" + customerPartitions);

    JSONObject customerPartitionsDetails = deltaHistory.getSparkTablePartitionDetail(sparkSession,
            "customer", "(product_name='shampoo', city='Delhi')" );
    System.out.println("............. spark Customer table Partitions details..............." + "\n"
            + customerPartitionsDetails);*/


//------------------------------------------------------------------------------------------------------------------------------

        //This methods give the partition detatil of delta table.
/*    sparkSession.sql("CREATE TABLE dextrus_delta_db.employee(e_id LONG, e_dep String, city String) USING delta PARTITIONED BY (e_dep,city)");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(1, 'java', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(2, 'scala', 'Noida')");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(3, 'java', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(4, 'scala', 'Noida')");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(5, 'java', 'Noida')");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(6, 'rust', 'Delhi')");
    sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(7, 'java', 'Noida')");

    JSONObject employeePartitionsDetails = deltaHistory.getDeltaTablePartitionDetail(sparkSession,
            "employee", "(e_dep='java', city='Delhi')" );
    System.out.println("............. delta employee table Partitions details..............." + "\n"
            + employeePartitionsDetails);*/


//        JSONObject sparkTableList = deltaHistory.getSparkTableList(sparkSession);
//        System.out.println(".............Spark tables..............." + "\n" + sparkTableList);
//        JSONObject customerPurchase = deltaHistory.getSparkTableStructure(sparkSession, "customerPurchase");
//        System.out.println("...........Spark table structure............" + customerPurchase);

//        JSONObject deltaTableStructure = deltaHistory.getDeltaTableStructure(sparkSession, 1, "customer");
//        System.out.println("..............Delta table structure............." + deltaTableStructure);


        //----------------------------------------------------------------------------------------------------------------------------

        // This two methods for getting versions of delta table and then getting the versioned table.
//        sparkSession.sql("CREATE TABLE dextrus_delta_db.employee(e_id LONG, e_dep String, city String) USING delta PARTITIONED BY (e_dep,city)");
//        sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(1, 'java', 'Delhi')");
//        sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(2, 'scala', 'Noida')");
//        sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(3, 'java', 'Delhi')");
//        sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(4, 'scala', 'Noida')");
//        sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(5, 'java', 'Noida')");
//        sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(6, 'rust', 'Delhi')");
//        sparkSession.sql("INSERT INTO dextrus_delta_db.employee VALUES(7, 'java', 'Noida')");
//
//
//        JSONObject historyOfTable = deltaHistory.
//                getHistoryOfTable(sparkSession, "employee");
//        System.out.println(historyOfTable);
//
//                JSONObject deltaTable = deltaHistory.getDeltaTable(sparkSession,
//                2, "employee");
//        System.out.println(deltaTable);

        //To get the spark execution plan.
//        SparkPlan sparkPlan = sparkSession.sql("CREATE TABLE dextrus_delta_db.employee(e_id LONG, e_dep String, city String) USING delta PARTITIONED BY (e_dep,city)").queryExecution().executedPlan();
//        System.out.println(sparkPlan);
//        sparkSession.close();
    }
}
