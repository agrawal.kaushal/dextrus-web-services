package com.rightdata.actuatorservice.Services;

import io.delta.tables.DeltaTable;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.plans.logical.LogicalPlan;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructField;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import scala.math.BigInt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

public class DeltaServiceUtil {

    /**
     * Get the list of parquet file with each size.
     *
     * @param filePath directory under which all files present.
     * @param sparkSession Spark session.
     * @return JSON Array of Json objects contains parquet file and size.
     */
    public JSONArray getParquetFilesAndSize(String filePath, SparkSession sparkSession) throws IOException {

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();

        Files.newDirectoryStream(Paths.get(filePath),
                path -> path.toString().endsWith(".parquet"))
                .forEach(file -> {
                            BigInt parquetSize = getParquetSize(file, sparkSession);
                            try {
                                jsonObject.put("fileName", (file.toString()).replaceAll(filePath + "/",""));
                                jsonObject.put("size", parquetSize);
                                jsonArray.put(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                );

        return jsonArray;
    }

    /**
     * Get the size of parquet file.
     *
     * @param file directory under which parquet file present.
     * @param sparkSession Spark session.
     * @return size of each parquet file.
     */
    public BigInt getParquetSize(Path file, SparkSession sparkSession) {

        Dataset<Row> rowDataset = sparkSession.read().parquet(file.toString()).toDF();
        LogicalPlan logical = rowDataset.queryExecution().logical();

        return sparkSession.sessionState().executePlan(
                logical).optimizedPlan().stats().sizeInBytes();

    }

    /**
     * Get the Result of DQL Statements.
     *
     * @param query Query to be passed.
     * @param sparkSession Spark session.
     * @return JSON Object contains result for DQL.
     */
    public JSONObject getResultForDQL(final SparkSession sparkSession,
                                      final String query) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        try {
            Iterator<String> stringIterator = sparkSession.sql(query).toJSON().toLocalIterator();
            String dataAsJsonString = getDataAsJsonString(stringIterator);

            StructField[] fields = sparkSession.sql(query).schema().fields();

            String columnsForData = getColumnsForData(fields);

            jsonObject.put("status", " DQL Spark Table Successful");
            jsonObject.put("data", dataAsJsonString);
            jsonObject.put("column", columnsForData);
        } catch (Exception exception) {
            jsonObject.put("status", "DQL Spark Table Failed");
            jsonObject.put("stackTrace",  exception.fillInStackTrace());
        }
        return jsonObject;
    }

    /**
     * Get the Logs against DDL and DMl Statements for spark Tables.
     *
     * @param query Query to be passed.
     * @param sparkSession Spark session.
     * @param ddlType Type of DDL like 'Create', 'Drop'.
     * @return JSON Object contains status and log Stack trace.
     */
    public JSONObject getLogForSparkTable(final SparkSession sparkSession,
                                          final String query,
                                          final String ddlType) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        try {
            sparkSession.sql(query);
            jsonObject.put("status", ddlType + " Spark Table Successful");
        } catch (Exception exception) {
            jsonObject.put("status", ddlType + " Spark Table Failed");
            jsonObject.put("stackTrace",  exception.fillInStackTrace());
        }
        return jsonObject;
    }

    /**
     * Get the Logs against DDL and DMl Statements for Delta Tables.
     *
     * @param query Query to be passed.
     * @param sparkSession Spark session.
     * @param ddlType Type of DDL like 'Create', 'Drop'.
     * @return JSON Object contains status and log Stack trace.
     */
    public JSONObject getLogForDeltaTableDDL(final SparkSession sparkSession,
                                             final String query,
                                             final String ddlType) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        try {
            sparkSession.sql(query);
            jsonObject.put("status", ddlType + " Delta Table Successful");
        } catch (Exception exception) {
            jsonObject.put("status", ddlType + " Delta Table Failed");
            jsonObject.put("stackTrace",  exception.fillInStackTrace());

        }
        return jsonObject;
    }

    /**
     * Get the Logs against DDL and DMl Statements for Delta Tables.
     *
     * @param partitionCondition partition condition like (e_dep='java', city='Delhi').
     * @param sparkSession Spark session.
     * @param table Delta table name.
     * @return JSON Object contains status and log Stack trace.
     */
    public JSONObject getDeltaTablePartitionDetail(final SparkSession sparkSession,
                                                   final String table,
                                                   final String partitionCondition) throws JSONException {

        Iterator<String> show_databases = sparkSession.sql(("DESCRIBE DETAIL tableName")
                .replaceAll("tableName",("dextrus_delta_db."+ table))
                .replaceAll("partitionInfo", partitionCondition))
                .toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(show_databases);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deltaPartitionDetail", dataAsJsonString);

        return jsonObject;
    }


    public JSONObject getDeltaTablePartitionInfo(final SparkSession sparkSession,
                                                 final String table ) throws JSONException {

        Iterator<String> show_databases = sparkSession.sql(("SHOW PARTITIONS tableName")
                .replaceAll("tableName",("dextrus_delta_db."+ table)))
                .toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(show_databases);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deltaPartitionInfo", dataAsJsonString);

        return jsonObject;
    }

    /**
     * Get the partiton info of spark table.
     *
     * @param sparkSession Spark session.
     * @param table delta table name.
     * @param partitionCondition partition Condition like "(product_name='shampoo', city='Delhi')".
     * @return JSON Object contains spark table partition info.
     */
    public JSONObject getSparkTablePartitionDetail(final SparkSession sparkSession,
                                                   final String table,
                                                   final String partitionCondition) throws JSONException {

        sparkSession.sql(("ANALYZE Table tableName PARTITION partitionInfo COMPUTE STATISTICS;")
                .replaceAll("tableName",("dextrus_spark_managed_db."+ table))
                .replaceAll("partitionInfo", partitionCondition));

        Iterator<String> show_databases = sparkSession.sql(("DESC EXTENDED tableName PARTITION partitionInfo")
                .replaceAll("tableName",("dextrus_spark_managed_db."+ table))
                .replaceAll("partitionInfo", partitionCondition))
                .filter(functions.col("col_name").equalTo("Partition Statistics"))
                .toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(show_databases);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sparkPartitionDetail", dataAsJsonString);

        return jsonObject;
    }

    /**
     * Get the partiton info of spark table.
     *
     * @param sparkSession Spark session.
     * @param table delta table name.
     * @return JSON Object contains spark table partition info.
     */
    public JSONObject getSparkTablePartitionInfo(final SparkSession sparkSession,
                                                 final String table ) throws JSONException {
        Iterator<String> show_databases = sparkSession.sql(("SHOW PARTITIONS tableName")
                .replaceAll("tableName",("dextrus_spark_managed_db."+ table)))
                .toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(show_databases);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sparkPartitionInfo", dataAsJsonString);

        return jsonObject;
    }

    /**
     * Get the structure of spark table.
     *
     * @param sparkSession Spark session.
     * @param table delta table name.
     * @return JSON Object contains spark table structure.
     */
    public JSONObject getSparkTableStructure(final SparkSession sparkSession,
                                             final String table  ) throws JSONException {

        Iterator<String> show_databases = sparkSession.sql(("DESCRIBE TABLE tableName")
                .replaceAll("tableName",("dextrus_spark_managed_db."+ table)))
                .toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(show_databases);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sparkTableStructure", dataAsJsonString);

        return jsonObject;
    }

    /**
     * Get the structure of delta table.
     *
     * @param sparkSession Spark session.
     * @param version version of table.
     * @param table delta table name.
     * @return JSON Object contains delta table structure.
     */
    public JSONObject getDeltaTableStructure(final SparkSession sparkSession,
                                             final Integer version,
                                             final String table  ) throws JSONException {


        final StructField[] fields = sparkSession.read().format("delta").option("versionAsOf", version)
                .table("dextrus_delta_db." + table).schema().fields();

        String columnAsJsonString = getColumnsForData(fields);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deltaTableStructure", columnAsJsonString);

        return jsonObject;
    }

    /**
     * Get the list of Databases.
     *
     * @param sparkSession Spark session.
     * @return JSON Object contains all databases.
     */
    public JSONObject getDatabasesList(final SparkSession sparkSession) throws JSONException {

        Iterator<String> show_databases = sparkSession.sql("SHOW Databases")
                .filter(functions.col("namespace").notEqual("default"))
                .toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(show_databases);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("databases", dataAsJsonString);

        return jsonObject;
    }

    /**
     * Get the list of Delta tables .
     *
     * @param sparkSession Spark session.
     * @return JSON Object contains all Delta tables.
     */
    public JSONObject getDeltaTableList(final SparkSession sparkSession) throws JSONException {

        Iterator<String> show_databases = sparkSession.sql("SHOW TABLES FROM dextrus_delta_db")
                .toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(show_databases);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deltaTables", dataAsJsonString);

        return jsonObject;
    }

    /**
     * Get the list of spark tables .
     *
     * @param sparkSession Spark session.
     * @return JSON Object contains all spark tables.
     */
    public JSONObject getSparkTableList(final SparkSession sparkSession) throws JSONException {

        Iterator<String> show_databases = sparkSession.sql("SHOW TABLES FROM dextrus_spark_managed_db")
                .toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(show_databases);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sparkTables", dataAsJsonString);

        return jsonObject;
    }

    /**
     * Get the delta table History detail containing the version.
     *
     * @param sparkSession Spark session.
     * @param deltaTable Delta table name.
     * @return JSON Object contains table name and version detail.
     */
    public JSONObject getHistoryOfTable(final SparkSession sparkSession,
                                        final String deltaTable) throws JSONException {

        DeltaTable salaryDeltaTable = DeltaTable.forName("dextrus_delta_db."+ deltaTable);
        salaryDeltaTable.history().toLocalIterator();

        Iterator<String> stringIterator = salaryDeltaTable.history()
                .select("version", "timestamp").toJSON().toLocalIterator();
        String dataAsJsonString = getDataAsJsonString(stringIterator);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tableName", deltaTable);
        jsonObject.put("versionDetails", dataAsJsonString );

        return jsonObject;
    }

    /**
     * Get the delta table of particular version.
     *
     * @param versionNumber version of table.
     * @param sparkSession Spark session.
     * @param deltaTable Delta table name.
     * @return JSON Object contains table data and column.
     */
    public JSONObject getDeltaTable(final SparkSession sparkSession,
                                    final Integer versionNumber,
                                    final String deltaTable) throws JSONException {

        Iterator<String> rowIterator = sparkSession.read().option("versionAsOf", versionNumber)
                .table("dextrus_delta_db." + deltaTable).toJSON().toLocalIterator();

        String dataAsJsonString = getDataAsJsonString(rowIterator);

        StructField[] fields = sparkSession.read().option("versionAsOf", versionNumber)
                .table("dextrus_delta_db." + deltaTable).schema().fields();

        String columnsForData = getColumnsForData(fields);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("column", columnsForData);
        jsonObject.put("data", dataAsJsonString );

        return jsonObject;
    }

    private String getDataAsJsonString(Iterator<String> iterator) {
        int count = 0;

        JSONArray jsonArray = new JSONArray();

        try {
            while (iterator.hasNext()) {
                if (count == (long) 1000)
                    break;
                JSONObject jsonObject = new JSONObject(iterator.next());
                jsonArray.put(jsonObject);
                count++;
            }
            return jsonArray.toString();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private String getColumnsForData(StructField[] fields) {
        JSONArray jsonArray = new JSONArray();
        try {
            for (StructField field : fields) {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("columnName", field.name());
                jsonObject.put("dataType", field.dataType().typeName());
                //jsonObject.put("length", JSONObject.NULL);
                //jsonObject.put("precision", JSONObject.NULL);
                jsonArray.put(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return jsonArray.toString();
    }
}
