package com.rightdata.actuatorservice.Services;

import com.rightdata.actuatorservice.POJOs.*;
import com.rightdata.actuatorservice.Utils.JwtTokenUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.*;
import java.util.Date;

@Service
public class QueryHistoryService {
    private final Logger logger = LoggerFactory.getLogger(QueryHistoryService.class);
    private long userId;

    public void saveQuery(QueryServicePOJO queryServicePOJO, String runQuery, long executionTime, String result, boolean isSuccess) {
        userId = JwtTokenUtil.userId;
        long clientId = JwtTokenUtil.clientId;
        Connection connection = createConnection();

        String guid = queryServicePOJO.getSource().getGuid();
        String connectionUrl = queryServicePOJO.getSource().getConnectionInfo().getConnectionJDBCURL();
        String userQuery = queryServicePOJO.getSource().getSourceInfo().getSourceTableOrQuery();
        int limit = queryServicePOJO.getSource().getPreviewCount();
        int offset = queryServicePOJO.getSource().getSkipRows();
        result = result.replaceAll("'", "\\\\'");

        try {
            Statement stmt = connection.createStatement();
            String updateQuery = "INSERT INTO `rightdata`.`query_history` (`clientId`, " +
                    " `userId`, `updatedAt`, `executionTime`, `clientName`, `userName`, `result`, `guid`, `userQuery`," +
                    " `runQuery`, `connectionUrl`, `limit`, `offset`, `isSuccess`) " +
                    "VALUES ('" + clientId + "', '" + userId + "', '" + new Date().getTime() + "', '" + executionTime + "', 'clientname', 'username', '" +
                    result + "', " +
                    "'" + guid + "', '" + userQuery + "', '" + runQuery + "', '" + connectionUrl + "', '" + limit + "'," +
                    " '" + offset + "', '" + (isSuccess ? 1 : 0) + "');";
            int affectedRows = stmt.executeUpdate(updateQuery);
            connection.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public ResponseEntity<QueryHistoryResponsePOJO> getQueryHistory() {
//        QueryServicePOJO queryServicePOJO, String clientId, String userId
        userId = JwtTokenUtil.userId;
        HttpHeaders responseHeaders = new HttpHeaders();
        QueryHistoryResponsePOJO responsePOJO = new QueryHistoryResponsePOJO();

        Connection connection = createConnection();

        try {
            Statement stmt = connection.createStatement();
//            ResultSet resultSet = stmt.executeQuery("select * from query_History qh where qh.clientId = " + 123 + " qh.userId = " + 123);
            ResultSet resultSet = stmt.executeQuery("select * from query_History qh where userId = " + userId);
            List<QueryHistoryPOJO> response = responseObjectMapper(resultSet);
            responsePOJO.setFlag(true);
            responsePOJO.setData(response);
            connection.close();
            return new ResponseEntity<QueryHistoryResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e);
            logger.error(e.getMessage());
            e.printStackTrace();
            responsePOJO.setFlag(false);
            responsePOJO.setResult(e.getMessage());
            if (e.getCause() != null) {
                responsePOJO.setFailureCause(e.getCause().toString());
            }
            return new ResponseEntity<QueryHistoryResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.CONFLICT);
        }

    }

    private List<QueryHistoryPOJO> responseObjectMapper(ResultSet resultSet) throws Exception {
        List<QueryHistoryPOJO> queryHistoryPOJOS = new ArrayList<QueryHistoryPOJO>();
        while(resultSet.next()) {
            QueryHistoryPOJO data = new QueryHistoryPOJO();

            data.runQuery = resultSet.getString("runQuery");
            data.userQuery = resultSet.getString("userQuery");
            data.connectionUrl = resultSet.getString("connectionUrl");
            data.executionTime = Long.parseLong(resultSet.getString("executionTime"));
            data.updatedAt = Long.parseLong(resultSet.getString("updatedAt"));
            data.result = resultSet.getString("result");
            data.isSuccess = Boolean.parseBoolean(resultSet.getString("isSuccess"));
            data.clientId = Long.parseLong(resultSet.getString("clientId"));
            data.clientName = resultSet.getString("clientName");
            data.userId = Long.parseLong(resultSet.getString("userId"));
            data.userName = resultSet.getString("userName");
            data.guid = resultSet.getString("guid");
            data.offset = Integer.parseInt(resultSet.getString("offset"));
            data.limit = Integer.parseInt(resultSet.getString("limit"));

            queryHistoryPOJOS.add(data);
        }
        return queryHistoryPOJOS;
    }


    private String getColumnDetails(ResultSetMetaData rsmd) {
        JSONArray jsonColumns = new JSONArray();
        try {
            int numColumns = 0;
            numColumns = rsmd.getColumnCount();
            for (int i=1; i<=numColumns; i++) {
                JSONObject columnObj = new JSONObject();
                String column_name = rsmd.getColumnLabel(i).toLowerCase();
                String column_type = rsmd.getColumnTypeName(i).toLowerCase();
                columnObj.put("columnName", column_name);
                columnObj.put("dataType", column_type);
                jsonColumns.put(columnObj);
            }
        } catch (Exception throwables) {
            logger.error(throwables.getMessage());
            throwables.printStackTrace();
        }
        return jsonColumns.toString();
    }

    protected Connection createConnection() {
        Connection remoteConn = null;

//        try {
//            Class.forName(connectionInfoPOJO.getClassName());
//        } catch (ClassNotFoundException cnf) {
//            logger.info("driver could not be loaded: " + cnf);
//        }

        try {
//            System.out.println("Connection JDBC URL: " + connectionInfoPOJO.getConnectionJDBCURL());
//            logger.info("Connection JDBC URL: " + connectionInfoPOJO.getConnectionJDBCURL());
//            if (connectionInfoPOJO.getConnectionJDBCURL().startsWith("jdbc:snowflake:")) {
                Properties properties = new java.util.Properties();
//                if (connectionInfoPOJO.getUsername() != null) {
//                    properties.put("user", connectionInfoPOJO.getUsername());
                    properties.put("user", "dextrus");
//                }
//                if (connectionInfoPOJO.getPassword() != null) {
//                    properties.put("password", connectionInfoPOJO.getPassword());
                    properties.put("password", "Dextrus!1");
//                }
//                if (connectionInfoPOJO.getAccount() != null) {
//                    properties.put("account", connectionInfoPOJO.getAccount());
//                }
//                if (connectionInfoPOJO.getWarehouse() != null) {
//                    properties.put("warehouse", connectionInfoPOJO.getWarehouse());
//                }
//                if (connectionInfoPOJO.getDb() != null) {
//                    properties.put("db", connectionInfoPOJO.getDb());
//                }
//                if (connectionInfoPOJO.getSchema() != null) {
//                    properties.put("schema", connectionInfoPOJO.getSchema());
//                }
//                if (connectionInfoPOJO.getRole() != null) {
//                    properties.put("role", connectionInfoPOJO.getRole());
//                }
//                remoteConn = DriverManager.getConnection(connectionInfoPOJO.getConnectionJDBCURL(), properties);
                remoteConn = DriverManager.getConnection("jdbc:mysql://54.145.80.245/rightdata?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", properties);
//            } else {
//                remoteConn = DriverManager.getConnection(connectionInfoPOJO.getConnectionJDBCURL(), connectionInfoPOJO.getUsername(), connectionInfoPOJO.getPassword());
//            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return remoteConn;
    }
}
