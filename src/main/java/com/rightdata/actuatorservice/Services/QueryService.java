package com.rightdata.actuatorservice.Services;

import com.rightdata.actuatorservice.POJOs.*;
import com.rightdata.actuatorservice.Utils.SparkSessionUtil;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
//import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import scala.collection.Map;
import scala.collection.mutable.HashMap;

import java.sql.*;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

@Service
public class QueryService {

    private final Logger logger = LoggerFactory.getLogger(QueryService.class);
    @Autowired
    public DeltaService deltaService;
    @Autowired
    public QueryHistoryService queryHistoryService;

    public ResponseEntity<ResponsePOJO> checkQuerySyntax(SourcePOJO sourcePOJO) {
        ResponseEntity<ResponsePOJO>  response = null;
        HttpHeaders responseHeaders = new HttpHeaders();
        ResponsePOJO responsePOJO = new ResponsePOJO();
        try {
            net.sf.jsqlparser.statement.Statement stmt = CCJSqlParserUtil.parse(sourcePOJO.getSourceInfo().getSourceTableOrQuery());
            responsePOJO.setFlag(true);
            responsePOJO.setResult("Query Syntax is correct");
        } catch (JSQLParserException e) {
            e.printStackTrace();
            responsePOJO.setFlag(false);
            responsePOJO.setResult(e.getMessage());
            if (e.getCause() != null) {
                responsePOJO.setFailureCause(e.getCause().toString());
            }
            return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);
        }
        return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);
    }

//    public StreamingResponseBody streamResponse(QueryServicePOJO queryServicePOJO) {
//        HttpHeaders responseHeaders = new HttpHeaders();
//        int limit = queryServicePOJO.getSource().getPreviewCount();
//        int offset = queryServicePOJO.getSource().getSkipRows();
//        String query = queryServicePOJO.getSource().getSourceInfo().getSourceTableOrQuery();
//        StreamingResponseBody stream = out -> {
//            try {
////            try(ObjectOutputStream oos = new ObjectOutputStream(out)) {
//                if (query.toUpperCase().startsWith("SELECT ")) {
//                    for (int i = 0; i < Math.ceil(limit*1.0/5000); i++) {
//                        String r = "";
//                        int l = 5000;
//                        if (i == Math.ceil(limit*1.0/5000) - 1) {
//                            l = (limit - i*5000);
//                        }
//                        queryServicePOJO.getSource().setPreviewCount(l);
//                        queryServicePOJO.getSource().setSkipRows(offset + i*5000);
//                        ResponseEntity<ResponsePOJO> response = processQuery(queryServicePOJO);
////                    if (!Objects.requireNonNull(response.getBody()).isFlag()) {
////                        out.write(new JSONObject(Objects.requireNonNull(response.getBody())).toString().getBytes());
////                        out.flush();
////                        out.close();
////                    }
////                        oos.write(queryServiceString.toString().getBytes());
////                    String content = "{\"counter\":" + i + "}\n";
////                    out.write(content.getBytes());
//                        ResponsePOJO responsePOJO = Objects.requireNonNull(response.getBody());
//                        responsePOJO.setCount(l);
////                        if (i == 0) {
////                            r += "[";
////                        } else {
////                            r = ",";
////                        }
//                        r += new JSONObject(responsePOJO).toString();
////                        if (i == (Math.ceil(limit*1.0/5000) - 1)) {
////                            r += "]";
////                        }
//                        out.write((r).getBytes());
////                        oos.flush();
//                        out.flush();
////                        out.close();
////                        logger.info("size: " + content.getBytes().length);
////                    Thread.sleep(1000);
//                    }
//                } else {
//                    ResponseEntity<ResponsePOJO> response = processQuery(queryServicePOJO);
//                    out.write(new JSONObject(Objects.requireNonNull(response.getBody())).toString().getBytes());
//                    out.flush();
//                }
////                for (int i = 0; i < 10; i++) {
////                    QueryServicePOJO queryServiceString =  new QueryServicePOJO();
//////                        oos.write(queryServiceString.toString().getBytes());
////                    String content = "{\"counter\":" + i + "}\n";
////                    out.write(content.getBytes());
////                    out.write(queryServiceString.toString().getBytes());
//////                        oos.flush();
////                    out.flush();
//////                        logger.info("size: " + content.getBytes().length);
////                    Thread.sleep(1000);
////                }
//            } catch (final Exception e) {
//                logger.error("Exception", e);
////                if (e.getLocalizedMessage().contains("connection was aborted")) {
//                    new Thread(() -> {
//                        queryHistoryService.saveQuery(queryServicePOJO, query, 0, e.getLocalizedMessage(), false);
//                    }).start();
////                }
//            }
//        };
//    return stream;
//
//    }

    public ResponseEntity<ResponsePOJO> processQuery(QueryServicePOJO queryServicePOJO) {
        HttpHeaders responseHeaders = new HttpHeaders();
        ResponsePOJO responsePOJO = new ResponsePOJO();

        ResponseEntity<ResponsePOJO> json;
        if (queryServicePOJO.getSource().getType().toUpperCase().equals("DELTA")) {
            json = deltaService.runQuery(queryServicePOJO);
        } else {
            queryServicePOJO.getSource().setGivenPreviewCount(queryServicePOJO.getSource().getPreviewCount());
            queryServicePOJO.getSource().setGivenSkipRows(queryServicePOJO.getSource().getSkipRows());
            if (queryServicePOJO.getSource().getPreviewCount() > 50000) {
                queryServicePOJO.getSource().setPreviewCount(50000);
            }
            json = RunQuery(queryServicePOJO);
        }
        System.out.println("Testing complete");
        return json;
    }

    protected ResponseEntity<ResponsePOJO> RunQuery(QueryServicePOJO pojo) {
        ResponseEntity<ResponsePOJO>  response = null;
        String purpose = pojo.getSource().getPurpose();

        if (purpose.equalsIgnoreCase("dataPreview")) {
            String sourceTableOrQuery = getSourceQuery(pojo);
            sourceTableOrQuery = checkQuery(pojo, sourceTableOrQuery);

            if (sourceTableOrQuery.toUpperCase().startsWith("SELECT ")) {
                response = getResponseFromSpark(pojo, sourceTableOrQuery);
            } else {
                response = getDMLResponse(pojo, sourceTableOrQuery);
            }
        }
        return response;
    }

    private String getSourceQuery(QueryServicePOJO pojo) {
        String query = "";
        SourceInfoPOJO sourceTableInfo = pojo.getSource().getSourceInfo();
        query = sourceTableInfo.getSourceTableOrQuery();
        logger.debug("Query is " + query);
        return query.toString();
    }

    private ResponseEntity<ResponsePOJO> getResponseFromSpark(QueryServicePOJO pojo, String sourceTableOrQuery) {
        HttpHeaders responseHeaders = new HttpHeaders();
        ResponsePOJO responsePOJO = new ResponsePOJO();
        int givenOffset = pojo.getSource().getGivenSkipRows();
        int givenLimit = pojo.getSource().getGivenPreviewCount();
        Statement stmt = null;
        int rowsAffected = 0;
        Dataset<Row> dataframe = null;
        ResultSetMetaData rsmd = null;
        long startTime = System.nanoTime();
        long endTime = System.nanoTime();
        try {
            dataframe = runSparkQuery(pojo, sourceTableOrQuery);
            endTime = System.nanoTime();
            List<String> collectionData = dataframe.toJSON().collectAsList();
//            Dataset<String> jsonDataset = dataframe.toJSON();
            String stringDataset = collectionData.toString();
            int count = collectionData.size();
            String columns = getColumnsForData(dataframe.schema().fields());

            responsePOJO.setFlag(true);
            responsePOJO.setSelectQuery(true);
            responsePOJO.setExecutionTime(endTime - startTime);
            responsePOJO.setData(stringDataset.toString());
            responsePOJO.setColumns(columns);
            responsePOJO.setCount(count);
            if (count == 50000 && givenLimit > count) {
                responsePOJO.setFetchNext(true);
            }
            responsePOJO.setOffset(givenOffset + count);
            responsePOJO.setLimit(givenLimit - count);
            long finalEndTime = endTime;
            new Thread(() -> {
                queryHistoryService.saveQuery(pojo, sourceTableOrQuery, finalEndTime - startTime, "success", true);
            }).start();
            return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            responsePOJO.setFlag(false);
            endTime = System.nanoTime();
            responsePOJO.setExecutionTime(endTime - startTime);
            responsePOJO.setResult(e.getMessage());
            if (e.getMessage().contains("No column name was specified for column")) {
                responsePOJO.setResult("Please provide proper column alias name.");
            }
            if (e.getCause() != null) {
                responsePOJO.setFailureCause(e.getCause().toString());
            }
            long finalEndTime1 = endTime;
            new Thread(() -> {
                queryHistoryService.saveQuery(pojo, sourceTableOrQuery, finalEndTime1 - startTime, e.getLocalizedMessage(), false);
            }).start();
            return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);
        }
    }

    private String getColumnsForData(StructField[] fields) {
        JSONArray jsonArray = new JSONArray();
        try {
            for (StructField field : fields) {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("columnName", field.name());
                jsonObject.put("dataType", field.dataType().typeName());
                //jsonObject.put("length", JSONObject.NULL);
                //jsonObject.put("precision", JSONObject.NULL);
                jsonArray.put(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return jsonArray.toString();
    }

    private ResponseEntity<ResponsePOJO> getDMLResponse(QueryServicePOJO pojo, String sourceTableOrQuery) {
        HttpHeaders responseHeaders = new HttpHeaders();
        ResponsePOJO responsePOJO = new ResponsePOJO();
        try {
            Connection conn = connectToRemoteDB(pojo.getSource().getConnectionInfo());
            Statement stmt = null;
            int rowsAffected = 0;
            ResultSet result = null;
            ResultSetMetaData rsmd = null;
            long startTime = System.nanoTime();
            long endTime = System.nanoTime();
            try {
                stmt = conn.createStatement();

                if (stmt.execute(sourceTableOrQuery)) {
                    endTime = System.nanoTime();
                    result = stmt.getResultSet();
                    rsmd = result.getMetaData();
                    JSONArray jsonResponse = new JSONArray();
                    int rows = 0;
                    while(result.next()) {
                        int numColumns = rsmd.getColumnCount();
                        JSONObject dataObj = new JSONObject();
                        for (int i=1; i<=numColumns; i++) {
                            String column_name = rsmd.getColumnLabel(i).toLowerCase();
                            dataObj.put(column_name, result.getObject(i));
                        }
                        rows += 1;
                        jsonResponse.put(dataObj);
                    }

                    responsePOJO.setRowsAffected(rows);
                    responsePOJO.setColumns(getColumnDetails(rsmd));
                    responsePOJO.setData(jsonResponse.toString());
                } else {
                    endTime = System.nanoTime();
                    rowsAffected = stmt.getUpdateCount();
                    responsePOJO.setResult(rowsAffected + " row(s) affected.");
                    if (rowsAffected < 0) {
                        responsePOJO.setResult("Success");
                    }
                    responsePOJO.setRowsAffected(stmt.getUpdateCount());
                }

                stmt.close();
                responsePOJO.setExecutionTime(endTime - startTime);
                responsePOJO.setFlag(true);
                long finalEndTime = endTime;
                int finalRowsAffected = rowsAffected;
                new Thread(() -> {
                    queryHistoryService.saveQuery(pojo, sourceTableOrQuery, finalEndTime - startTime, finalRowsAffected + " row(s) affected.", true);
                }).start();
                return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);
            } catch (SQLException e) {
                logger.error(e.getMessage());
                e.printStackTrace();
                responsePOJO.setFlag(false);
                endTime = System.nanoTime();
                responsePOJO.setExecutionTime(endTime - startTime);
                responsePOJO.setResult(e.getMessage());
                if (e.getCause() != null) {
                    responsePOJO.setFailureCause(e.getCause().toString());
                }
                long finalEndTime1 = endTime;
                new Thread(() -> {
                    queryHistoryService.saveQuery(pojo, sourceTableOrQuery, finalEndTime1 - startTime, e.getLocalizedMessage(), false);
                }).start();
                return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.OK);
            } finally {
                try {
                    if (stmt != null)
                        stmt.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage());
                    e.printStackTrace();
                    responsePOJO.setFlag(false);
                    responsePOJO.setResult(e.getMessage());
                    if (e.getCause() != null) {
                        responsePOJO.setFailureCause(e.getCause().toString());
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            responsePOJO.setFlag(false);
            responsePOJO.setResult(e.getMessage());
            if (e.getCause() != null) {
                responsePOJO.setFailureCause(e.getCause().toString());
            }
        }
        return new ResponseEntity<ResponsePOJO>(responsePOJO, responseHeaders, HttpStatus.BAD_REQUEST);
    }

    private String checkQuery(QueryServicePOJO pojo, String sourceTableOrQuery) {
        sourceTableOrQuery = sourceTableOrQuery.trim();
        String connectionJDBCURL = pojo.getSource().getConnectionInfo().getConnectionJDBCURL();
        if(connectionJDBCURL.startsWith("jdbc:sqlserver:")) {
            SqlServerService sqlServerService = new SqlServerService();
            sourceTableOrQuery = sqlServerService.handleQuery(pojo, sourceTableOrQuery);

        } else if (connectionJDBCURL.startsWith("jdbc:snowflake:")) {
            SnowflakeQueryService snowflakeQueryService = new SnowflakeQueryService();
            sourceTableOrQuery = snowflakeQueryService.handleQuery(pojo, sourceTableOrQuery);

        } else if (connectionJDBCURL.startsWith("jdbc:oracle:")) {
            OracleQueryService oracleQueryService = new OracleQueryService();
            sourceTableOrQuery = oracleQueryService.handleQuery(pojo, sourceTableOrQuery);

        } else if (connectionJDBCURL.startsWith("jdbc:redshift:")) {
            RedshiftQueryService redshiftQueryService = new RedshiftQueryService();
            sourceTableOrQuery = redshiftQueryService.handleQuery(pojo, sourceTableOrQuery);

        } else if (connectionJDBCURL.startsWith("jdbc:mysql:")) {
            MysqlQueryService mysqlQueryService = new MysqlQueryService();
            sourceTableOrQuery = mysqlQueryService.handleQuery(pojo, sourceTableOrQuery);
        }
        return sourceTableOrQuery;
    }

    public Dataset<Row> runSparkQuery(QueryServicePOJO pojo, String query) throws Exception {
        try {
            SparkSession sparkSession = SparkSessionUtil.sparkSession;
            String username = pojo.getSource().getConnectionInfo().getUsername();
            String password = pojo.getSource().getConnectionInfo().getPassword();
            String url = pojo.getSource().getConnectionInfo().getConnectionJDBCURL();
            String className = pojo.getSource().getConnectionInfo().getClassName();
            String account = pojo.getSource().getConnectionInfo().getAccount();
            String warehouse = pojo.getSource().getConnectionInfo().getWarehouse();
            String db = pojo.getSource().getConnectionInfo().getDb();
            String schema = pojo.getSource().getConnectionInfo().getSchema();
            String role = pojo.getSource().getConnectionInfo().getRole();

            Properties properties = new Properties();
            if (username != null) {
                properties.put("user", username);
            }
            if (password != null) {
                properties.put("password", password);
            }
            if (className != null) {
                properties.setProperty("driver", className);
            }
            if (pojo.getSource().getConnectionInfo().getConnectionJDBCURL().startsWith("jdbc:snowflake:")) {
                if (account != null) {
                    properties.put("account", account);
                }
                if (warehouse != null) {
                    properties.put("warehouse", warehouse);
                }
                if (db != null) {
                    properties.put("db", db);
                }
                if (schema != null) {
                    properties.put("schema", schema);
                }
                if (role != null) {
                    properties.put("role", role);
                }
            }
//            properties.put("dbtable", query);

            query = query.replaceAll(";", "");
            query = "(" + query + ") table_name";
            logger.info("Remote query: " + query);

            Map<String, String> rightHereMap = new HashMap<>();
            ((HashMap<String, String>) rightHereMap).put("user", username);
            ((HashMap<String, String>) rightHereMap).put("password", password);
            ((HashMap<String, String>) rightHereMap).put("database", db);
            ((HashMap<String, String>) rightHereMap).put("driver", className);
            ((HashMap<String, String>) rightHereMap).put("url", url);
            ((HashMap<String, String>) rightHereMap).put("dbtable", "tb1");



//            Dataset<Row> stream = sparkSession.read().format("jdbc").options(rightHereMap).load().writeStream();
//            stream.writeStream()
//                    .outputMode("append")
//                    .format("console")
//                    .start();
//            return sparkSession.read()
//                    .format("jdbc")
//                    .option("driver" , className)
//                    .option("url", url)
//                    .option("dbtable", query)
//                    .option("user", username)
//                    .option("password", password)
//                    .load();
            return sparkSession.read().jdbc(url, query, properties);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    private boolean isSelectQuery(String connectionJDBCURL, String sourceTableOrQuery) {
        if(connectionJDBCURL.startsWith("jdbc:sqlserver:")) {
            if (sourceTableOrQuery.toUpperCase().startsWith("SELECT ")
                    || sourceTableOrQuery.toUpperCase().startsWith("WITH ")
                    || sourceTableOrQuery.toUpperCase().startsWith("SHOW ")
                    || sourceTableOrQuery.toUpperCase().startsWith("EXEC ")
                    || sourceTableOrQuery.toUpperCase().startsWith("DESCRIBE ")) {
                return true;
            }
        } else if (connectionJDBCURL.startsWith("jdbc:snowflake:")) {
            if (sourceTableOrQuery.toUpperCase().startsWith("SELECT ")
                    || sourceTableOrQuery.toUpperCase().startsWith("WITH ")
                    || sourceTableOrQuery.toUpperCase().startsWith("SHOW ")
                    || sourceTableOrQuery.toUpperCase().startsWith("EXEC ")
                    || sourceTableOrQuery.toUpperCase().startsWith("DESCRIBE ")) {
                return true;
            }
        } else if (connectionJDBCURL.startsWith("jdbc:oracle:")) {
            if (sourceTableOrQuery.toUpperCase().startsWith("SELECT ")
                    || sourceTableOrQuery.toUpperCase().startsWith("WITH ")
                    || sourceTableOrQuery.toUpperCase().startsWith("SHOW ")
                    || sourceTableOrQuery.toUpperCase().startsWith("EXEC ")
                    || sourceTableOrQuery.toUpperCase().startsWith("DESCRIBE ")) {
                return true;
            }
        } else if (connectionJDBCURL.startsWith("jdbc:redshift:")) {
            if (sourceTableOrQuery.toUpperCase().startsWith("SELECT ")
                    || sourceTableOrQuery.toUpperCase().startsWith("WITH ")
                    || sourceTableOrQuery.toUpperCase().startsWith("SHOW ")
                    || sourceTableOrQuery.toUpperCase().startsWith("EXEC ")
                    || sourceTableOrQuery.toUpperCase().startsWith("DESCRIBE ")) {
                return true;
            }
        } else if (connectionJDBCURL.startsWith("jdbc:mysql:")) {
            if (sourceTableOrQuery.toUpperCase().startsWith("SELECT ")
                    || sourceTableOrQuery.toUpperCase().startsWith("WITH ")
                    || sourceTableOrQuery.toUpperCase().startsWith("SHOW ")
                    || sourceTableOrQuery.toUpperCase().startsWith("EXEC ")
                    || sourceTableOrQuery.toUpperCase().startsWith("DESCRIBE ")) {
                return true;
            }
        }
        return false;
    }

    private String getColumnDetails(ResultSetMetaData rsmd) {
        JSONArray jsonColumns = new JSONArray();
        try {
            int numColumns = 0;
            numColumns = rsmd.getColumnCount();
            for (int i=1; i<=numColumns; i++) {
                JSONObject columnObj = new JSONObject();
                String column_name = rsmd.getColumnLabel(i).toLowerCase();
                String column_type = rsmd.getColumnTypeName(i).toLowerCase();
                columnObj.put("columnName", column_name);
                columnObj.put("dataType", column_type);
                jsonColumns.put(columnObj);
            }
        } catch (Exception throwables) {
            logger.error(throwables.getMessage());
            throwables.printStackTrace();
        }
        return jsonColumns.toString();
    }

    protected Connection connectToRemoteDB(ConnectionInfoPOJO connectionInfoPOJO) {
        Connection remoteConn = null;

        try {
            Class.forName(connectionInfoPOJO.getClassName());
        } catch (ClassNotFoundException cnf) {
            logger.info("driver could not be loaded: " + cnf);
        }

        try {
            System.out.println("Connection JDBC URL: " + connectionInfoPOJO.getConnectionJDBCURL());
            logger.info("Connection JDBC URL: " + connectionInfoPOJO.getConnectionJDBCURL());
            if (connectionInfoPOJO.getConnectionJDBCURL().startsWith("jdbc:snowflake:")) {
                Properties properties = new java.util.Properties();
                if (connectionInfoPOJO.getUsername() != null) {
                    properties.put("user", connectionInfoPOJO.getUsername());
                }
                if (connectionInfoPOJO.getPassword() != null) {
                    properties.put("password", connectionInfoPOJO.getPassword());
                }
                if (connectionInfoPOJO.getAccount() != null) {
                    properties.put("account", connectionInfoPOJO.getAccount());
                }
                if (connectionInfoPOJO.getWarehouse() != null) {
                    properties.put("warehouse", connectionInfoPOJO.getWarehouse());
                }
                if (connectionInfoPOJO.getDb() != null) {
                    properties.put("db", connectionInfoPOJO.getDb());
                }
                if (connectionInfoPOJO.getSchema() != null) {
                    properties.put("schema", connectionInfoPOJO.getSchema());
                }
                if (connectionInfoPOJO.getRole() != null) {
                    properties.put("role", connectionInfoPOJO.getRole());
                }
                remoteConn = DriverManager.getConnection(connectionInfoPOJO.getConnectionJDBCURL(), properties);
            } else {
                remoteConn = DriverManager.getConnection(connectionInfoPOJO.getConnectionJDBCURL(), connectionInfoPOJO.getUsername(), connectionInfoPOJO.getPassword());
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return remoteConn;
    }

    public boolean isMSSQLServerVer2012OrLater(Connection con) {
        Statement statement;
        ResultSet rs;
        try {
            statement = con.createStatement();
            rs = statement.executeQuery("SELECT" +
                    "  CASE " +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '8%' THEN 'SQL2000'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '9%' THEN 'SQL2005'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '10.0%' THEN 'SQL2008'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '10.5%' THEN 'SQL2008 R2'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '11%' THEN 'SQL2012'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '12%' THEN 'SQL2014'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '13%' THEN 'SQL2016'" +
                    "     WHEN CONVERT(VARCHAR(128), SERVERPROPERTY ('productversion')) like '14%' THEN 'SQL2017'" +
                    "     ELSE 'unknown'" +
                    "  END AS MajorVersion");
            if (rs.next()) {
                String version = rs.getString("MajorVersion");
                logger.info("MSSQL Version: " + version);
                return version.equalsIgnoreCase("SQL2012") || version.equalsIgnoreCase("SQL2014") || version.equalsIgnoreCase("SQL2016") || version.equalsIgnoreCase("SQL2017");
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
}
