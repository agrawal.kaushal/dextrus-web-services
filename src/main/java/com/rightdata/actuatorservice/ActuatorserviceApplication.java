package com.rightdata.actuatorservice;

import com.rightdata.actuatorservice.Utils.SparkSessionUtil;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.io.File;

@SpringBootApplication
public class ActuatorserviceApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ActuatorserviceApplication.class, args);
		SparkSessionUtil.startSession();
	}


	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ActuatorserviceApplication.class);
	}

}
