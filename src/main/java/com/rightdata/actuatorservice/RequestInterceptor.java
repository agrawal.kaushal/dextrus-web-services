//package com.rightdata.actuatorservice;
//
//import com.rightdata.actuatorservice.Services.LogoutService;
//import com.rightdata.actuatorservice.Utils.JwtTokenUtil;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Date;
//
//@Component
//public class RequestInterceptor extends HandlerInterceptorAdapter {
//
//    private transient Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);
//
//    @Autowired
//    private LogoutService logoutService;
//
//    @Autowired
//    private transient JwtTokenUtil jwtTokenUtil;
//
//    @Override
//    public boolean preHandle(
//            HttpServletRequest request,
//            HttpServletResponse response,
//            Object handler)  {
//        String authToken = request.getHeaders("Authorization").nextElement();
//        String expirationTime = String.valueOf(jwtTokenUtil.getExpirationDateFromToken(authToken).getTime());
//        boolean isBlacklisted = logoutService.checkBlacklist(authToken, expirationTime);
//        if(isBlacklisted)
//            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
//        return !isBlacklisted;
//    }
//
//    @Override
//    public void postHandle(
//            HttpServletRequest request,
//            HttpServletResponse response,
//            Object handler,
//            ModelAndView modelAndView) {
//
//        logger.info(new Date() + " Request successfully completed");
//    }
//
//    @Override
//    public void afterCompletion
//            (HttpServletRequest request,
//             HttpServletResponse response,
//             Object handler,
//             Exception exception) {
//
//    }
//
//}
